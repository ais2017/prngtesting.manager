//
// Created by svuatoslav on 11/4/18.
//

#ifndef PRNG_MANAGER_MANAGER_H
#define PRNG_MANAGER_MANAGER_H

#include "ModuleInterface.h"
#include "ControlInterface.h"
#include "StorageInterface.h"

#include "ModuleQueue.h"

class Manager
{
    std::shared_ptr<ControlInterface> ctli;
    std::shared_ptr<StorageInterface> stgi;
    std::unordered_map<int, std::shared_ptr<ModuleInterface>> ifaces;

    bool checkConfig(const std::vector<std::pair<int, int>> &config) const;

    bool checkModules (const std::vector<std::pair<int, int>> &config,
                      const std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> &modulesData,
                      const std::pair<int, std::vector<std::pair<std::string, std::string>>> &saveModule) const;

    bool checkAll (const std::vector<std::pair<int, int>> &config,
                      const std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> &modulesData,
                      const std::pair<int, std::vector<std::pair<std::string, std::string>>> &saveModule) const;

public:

    Manager(const std::shared_ptr<ControlInterface> &ctli, const std::shared_ptr<StorageInterface> &stgi,
                const std::unordered_map<int, std::shared_ptr<ModuleInterface>> &ifaces);

    bool setup(const std::vector<std::pair<int, int>> &config);

    bool setModules(const std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> &modulesData,
                    const std::pair<int, std::vector<std::pair<std::string, std::string>>> &saveModule);

    void launch();

    void save();
};


#endif //PRNG_MANAGER_MANAGER_H
