//
// Created by svuatoslav on 10/18/18.
//

#ifndef PRNG_MANAGER_MODULEQUEUE_H
#define PRNG_MANAGER_MODULEQUEUE_H

#include <Module.h>
#include <deque>

class ModuleQueue : private std::deque<std::shared_ptr<Module>>
{
public:

    /*!
     * Super class type
     */
    typedef std::deque<std::shared_ptr<Module>> super;

    /*!
     * Constructor
     */
    ModuleQueue () = default;


    /*!
     * Copy constructor
     * @param Queue to copy from
     * @throws std::bad_alloc
     */
    ModuleQueue (const ModuleQueue &other);


    /*!
     * Move constructor
     * @param other Queue to move
     */
    ModuleQueue (ModuleQueue &&other) = default;


    /*!
     * Destructor
     */
    ~ModuleQueue () noexcept = default;


    /*!
     * Add a new module to the queue
     * @param params Module parameters
     * @param type Module type
     * @return Created module
     * @throws std::bad_alloc
     */
    std::weak_ptr<const Module> add (const std::vector<std::pair<std::string, std::string>> &params, int type);

    /*!
     * Add a new module to the queue with no parameters
     * @param type Module type
     * @return Created module
     * @throws std::bad_alloc
     */
    std::weak_ptr<const Module> add (int type);


    /*!
     * Removes module from the queue
     * @param module Module to remove
     */
    void remove (const std::weak_ptr<const Module> &module) noexcept;


    /*!
     * Removes all modules with specified status from the queue
     * @param status Status of modules to remove
     */
    void remove (Module::Status status) noexcept;


    /*!
     * Removes all modules from the queue
     */
    void remove () noexcept;


    /*!
     * Get first module from the queue
     * @return Module or nullptr if queue is empty
     */
    std::weak_ptr<const Module> first () const noexcept;


    /*!
     * Get first module with specified status from the queue
     * @return Module or nullptr if no such modules found
     */
    std::weak_ptr<const Module> first (Module::Status status) const noexcept;


    /*!
     * Check if the queue is empty
     * @return True if queue is empty, false otherwise
     */
    bool isEmpty () const noexcept;


    /*!
     * Set module's parameter
     * @param module Module
     * @param name Parameter name
     * @param value Parameter value
     * @throws std::logic_error If module was not found in the queue
     * @throws std::bad_alloc
     */
    void setParam (const std::weak_ptr<const Module> &module, const std::string &name, const std::string &value);


    /*!
     * Set module's status
     * @param module Module
     * @param status Status
     * @throws std::logic_error If module was not found in the queue
     */
    void setStatus (const std::weak_ptr<const Module> &module, Module::Status status);

};


#endif //PRNG_MANAGER_MODULEQUEUE_H
