//
// Created by svuatoslav on 10/18/18.
//

#include "ModuleQueue.h"

ModuleQueue::ModuleQueue (const ModuleQueue &other) : super ()
{
    for (auto &it : other)
    {
        push_back (std::make_shared<Module> (*it));
    }
}


std::weak_ptr<const Module> ModuleQueue::add (const std::vector<std::pair<std::string, std::string>> &params, int type)
{
    push_back (std::make_shared<Module> (params, type));
    return std::weak_ptr<Module> (back ());
}

std::weak_ptr<const Module> ModuleQueue::add (int type)
{
    push_back (std::make_shared<Module> (type));
    return std::weak_ptr<const Module> (back ());
}

void ModuleQueue::remove (const std::weak_ptr<const Module> &module) noexcept
{
    for (auto it = begin (); it != end (); ++it)
    {
        if (*it == module.lock ())
        {
            erase (it);
            return;
        }
    }
}

void ModuleQueue::remove (Module::Status status) noexcept
{
    for (auto it = begin (); it != end ();)
    {
        if ((*it)->getStatus () == status)
        {
            it = erase (it);
        }
        else
        {
            ++it;
        }
    }
}

void ModuleQueue::remove () noexcept
{
    clear ();
}

std::weak_ptr<const Module> ModuleQueue::first () const noexcept
{
    if (empty ())
        return std::weak_ptr<const Module> ();
    return std::weak_ptr<const Module> (front ());
}

std::weak_ptr<const Module> ModuleQueue::first (Module::Status status) const noexcept
{
    for (const auto &it : *this)
    {
        if (it->getStatus () == status)
        {
            return std::weak_ptr<const Module> (it);
        }
    }
    return std::weak_ptr<const Module> ();
}

bool ModuleQueue::isEmpty () const noexcept
{
    return empty ();
}

void
ModuleQueue::setParam (const std::weak_ptr<const Module> &module, const std::string &name, const std::string &value)
{
    for (const auto &it : *this)
    {
        if (it == module.lock ())
        {
            it->setParam (name, value);
            return;
        }
    }
    throw std::logic_error ("Module was not found in the queue");
}

void ModuleQueue::setStatus (const std::weak_ptr<const Module> &module, Module::Status status)
{
    for (const auto &it : *this)
    {
        if (it == module.lock ())
        {
            it->setStatus (status);
            return;
        }
    }
    throw std::logic_error ("Module was not found in the queue");
}