//
// Created by svuatoslav on 10/18/18.
//
#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <unordered_set>
#include "ModuleQueue.h"

const char *randstr ()
{
    static char buf[1024];
    size_t len = rand () % (sizeof (buf) / sizeof (char));
    for (size_t i = 0; i < len; i += sizeof (long) / sizeof (char))
    {
        if (len - i >= sizeof (long) / sizeof (char))
        {
            *((long *) (buf + i)) = rand ();
        }
        else
        {
            long rndval = rand ();
            memcpy (buf + i, &rndval, (len - i) * sizeof (char));
        }
    }
    return buf;
}

TEST_CASE ("Module Queue Test")
{
    srand (static_cast<unsigned int>(time (nullptr)));

    SECTION ("Construct And Destruct")
    {
        ModuleQueue *queue = nullptr;
        REQUIRE_NOTHROW (queue = new ModuleQueue);
        REQUIRE_NOTHROW (delete queue);
    }

    SECTION("Add Modules")
    {
        ModuleQueue queue;
        SECTION("From Type")
        {
            SECTION ("Add One Module")
            {
                auto type = static_cast<int>(rand ());
                std::weak_ptr<const Module> module;
                REQUIRE_NOTHROW (module = queue.add (type));
                REQUIRE_FALSE (module.expired ());
                REQUIRE (module.lock ()->getType () == type);
                REQUIRE (module.lock ()->getParam (randstr ()).empty ());
            }
            SECTION ("Add Many Modules")
            {
                auto num = static_cast<size_t>(rand () % 1024 + 2);
                std::vector<int> types (num);
                std::deque<std::weak_ptr<const Module>> modules;
                for (auto &i : types)
                {
                    i = static_cast<int>(rand ());
                    REQUIRE_NOTHROW (modules.push_back (queue.add (i)));
                }
                for (auto &i : types)
                {
                    REQUIRE_FALSE (modules.front ().expired ());
                    REQUIRE (modules.front ().lock ()->getType () == i);
                    REQUIRE (modules.front ().lock ()->getParam (randstr ()).empty ());
                    modules.pop_front ();
                }
            }
        }
        SECTION("From Type And Params")
        {
            SECTION ("Add One Module With Params")
            {
                auto type = static_cast<int>(rand ());
                std::vector<std::pair<std::string, std::string>> paramsv;
                {
                    std::unordered_set<std::string> params;
                    size_t paramsnum = static_cast<size_t>(rand ()) % 1024;
                    paramsv.reserve (paramsnum);
                    for (size_t i = 0; i < paramsnum; ++i)
                    {
                        std::string name (randstr ());
                        std::string value (randstr ());
                        if (!name.empty () && params.find (name) == params.end ())
                        {
                            params.insert (name);
                            paramsv.emplace_back (name, value);
                        }
                    }
                }
                std::weak_ptr<const Module> module;
                REQUIRE_NOTHROW (module = queue.add (paramsv, type));
                REQUIRE_FALSE (module.expired ());
                REQUIRE (module.lock ()->getType () == type);
                for (auto &i : paramsv)
                {
                    REQUIRE (module.lock ()->getParam (i.first) == i.second);
                }
            }
            SECTION ("Add Many Modules With Params")
            {
                auto num = static_cast<size_t>(rand () % 1024 + 2);
                std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> mods (num);
                std::deque<std::weak_ptr<const Module>> modules;
                for (auto &mod : mods)
                {
                    mod.first = static_cast<int>(rand ());
                    {
                        std::unordered_set<std::string> params;
                        size_t paramsnum = static_cast<size_t>(rand ()) % 1024;
                        mod.second.reserve (paramsnum);
                        for (size_t i = 0; i < paramsnum; ++i)
                        {
                            std::string name (randstr ());
                            std::string value (randstr ());
                            if (!name.empty () && params.find (name) == params.end ())
                            {
                                params.insert (name);
                                mod.second.emplace_back (name, value);
                            }
                        }
                    }
                    std::weak_ptr<Module> module;
                    REQUIRE_NOTHROW (modules.push_back (queue.add (mod.second, mod.first)));
                }
                for (auto &i : mods)
                {
                    REQUIRE_FALSE (modules.front ().expired ());
                    REQUIRE (modules.front ().lock ()->getType () == i.first);
                    for (auto &param : i.second)
                    {
                        REQUIRE (modules.front ().lock ()->getParam (param.first) == param.second);
                    }
                    modules.pop_front ();
                }
            }
        }
    }

    SECTION ("Params")
    {
        ModuleQueue queue;
        SECTION ("Set Param")
        {
            auto num = static_cast<size_t>(rand () % 1024 + 2);
            std::vector<int> types (num);
            std::deque<std::weak_ptr<const Module>> modules;
            for (auto &i : types)
            {
                i = static_cast<int>(rand ());
                REQUIRE_NOTHROW (modules.push_back (queue.add (i)));
            }
            std::vector<std::pair<std::string, std::string>> paramsv;
            {
                std::unordered_set<std::string> params;
                size_t paramsnum = static_cast<size_t>(rand ()) % 1024;
                paramsv.reserve (paramsnum);
                for (size_t i = 0; i < paramsnum; ++i)
                {
                    std::string name (randstr ());
                    std::string value (randstr ());
                    if (!name.empty () && params.find (name) == params.end ())
                    {
                        params.insert (name);
                        paramsv.emplace_back (name, value);
                    }
                }
            }
            auto module = modules[rand () % modules.size ()];
            for (auto &i : paramsv)
            {
                REQUIRE_NOTHROW (queue.setParam (module, i.first, i.second));
            }
            for (auto &i : paramsv)
            {
                REQUIRE (module.lock ()->getParam (i.first) == i.second);
            }
        }
    }

    SECTION ("Set Status")
    {
        ModuleQueue queue;
        auto num = static_cast<size_t>(rand () % 1024 + 2);
        std::vector<int> types (num);
        std::deque<std::weak_ptr<const Module>> modules;
        for (auto &i : types)
        {
            i = static_cast<int>(rand ());
            REQUIRE_NOTHROW (modules.push_back (queue.add (i)));
        }
        auto module = modules[rand () % modules.size ()];
        SECTION ("Set Correct")
        {
            REQUIRE (module.lock ()->getStatus () == Module::Waiting);
            REQUIRE_NOTHROW (queue.setStatus (module, Module::Working));
            REQUIRE (module.lock ()->getStatus () == Module::Working);
        }
        SECTION ("Set Incorrect")
        {
            REQUIRE (module.lock ()->getStatus () == Module::Waiting);
            REQUIRE_THROWS_AS (queue.setStatus (module, Module::Failed), std::logic_error);
            REQUIRE (module.lock ()->getStatus () == Module::Waiting);
        }
    }

    SECTION ("Remove")
    {
        ModuleQueue queue;
        auto num = static_cast<size_t>(rand () % 1024 + 1);
        std::deque<std::weak_ptr<const Module>> modules;
        for (size_t i = 0; i < num; ++i)
        {
            REQUIRE_NOTHROW (modules.push_back (queue.add (static_cast<int>(rand ()))));
        }
        SECTION ("Remove By One")
        {
            while (!modules.empty ())
            {
                REQUIRE_FALSE (queue.isEmpty ());
                size_t i = rand () % modules.size ();
                auto module = modules[i];
                REQUIRE_FALSE (module.expired ());
                REQUIRE_NOTHROW (queue.remove (module));
                REQUIRE (module.expired ());
                modules.erase (modules.begin () + i);
            }
            REQUIRE (queue.isEmpty ());
        }
        SECTION ("Remove By Status")
        {
            auto stupdatenum = static_cast<size_t>(rand () % 1024);
            for (size_t i = 0; i < stupdatenum; ++i)
            {
                size_t modnum = rand () % modules.size ();
                auto module = modules[modnum];
                switch (module.lock ()->getStatus ())
                {
                    case Module::Waiting:
                        queue.setStatus (module, Module::Working);
                        break;
                    case Module::Working:
                        if (rand () % 2)
                        {
                            queue.setStatus (module, Module::Failed);
                        }
                        else
                        {
                            queue.setStatus (module, Module::Completed);
                        }
                        break;
                    default:
                        break;
                }
            }
            int cleared = 0;
            while (cleared != 15)
            {
                int statusno;
                do
                {
                    statusno = static_cast<uint8_t>(rand () % 4);
                } while (cleared & (1 << statusno));
                cleared |= (1 << statusno);
                Module::Status status;
                switch (statusno)
                {
                    case 0:
                        status = Module::Waiting;
                        break;
                    case 1:
                        status = Module::Working;
                        break;
                    case 2:
                        status = Module::Completed;
                        break;
                    case 3:
                        status = Module::Failed;
                        break;
                    default:
                        abort (); //Should not get here
                }
                typeof (modules) statmods;
                for (auto it = modules.begin (); it != modules.end ();)
                {
                    REQUIRE_FALSE (it->expired ());
                    if (it->lock ()->getStatus () == status)
                    {
                        statmods.push_back (*it);
                        it = modules.erase (it);
                    }
                    else
                    {
                        ++it;
                    }
                }
                REQUIRE_FALSE ((queue.isEmpty () && !statmods.empty ()));
                REQUIRE_NOTHROW (queue.remove (status));
                for (auto &i : statmods)
                {
                    REQUIRE (i.expired ());
                }
            }
            REQUIRE (queue.isEmpty ());
        }
        SECTION ("Remove All")
        {
            REQUIRE_FALSE (queue.isEmpty ());
            REQUIRE_NOTHROW (queue.remove ());
            REQUIRE (queue.isEmpty ());
        }
        SECTION ("Remove Expired")
        {
            auto module = queue.first ();
            queue.remove();
            REQUIRE_NOTHROW (queue.remove (module));
        }
        SECTION ("Remove Nullptr")
        {
            queue.remove();
            REQUIRE_NOTHROW (queue.remove (std::weak_ptr<const Module> (std::shared_ptr<Module> (nullptr))));
        }
    }

    SECTION ("Get")
    {
        ModuleQueue queue;
        SECTION ("Get First")
        {
            SECTION ("Not Contained")
            {
                REQUIRE (queue.first ().expired ());
            }
            SECTION ("Contained")
            {
                auto num = static_cast<size_t>((rand () % 1024) + 1);
                std::deque<std::weak_ptr<const Module>> modules;
                for (size_t i = 0; i < num; ++i)
                {
                    REQUIRE_NOTHROW (modules.push_back (queue.add (static_cast<int>(rand ()))));
                }
                while (!modules.empty ())
                {
                    std::weak_ptr<const Module> module;
                    REQUIRE_NOTHROW (module = queue.first ());
                    REQUIRE_FALSE (module.expired ());
                    REQUIRE_FALSE (modules.front ().expired ());
                    REQUIRE (module.lock () == modules.front ().lock ());
                    REQUIRE_NOTHROW (queue.remove (module));
                    REQUIRE (module.expired ());
                    modules.pop_front ();
                }
                REQUIRE (queue.first ().expired ());
                REQUIRE (queue.isEmpty ());
            }
        }
        SECTION ("Get First With Status")
        {
            SECTION ("Not Contained")
            {
                queue.remove ();
                REQUIRE (queue.first ((Module::Status)(rand()%4)).expired ());
            }
            SECTION ("Contained")
            {
                auto num = static_cast<size_t>((rand () % 1024) + 1);
                std::deque<std::weak_ptr<const Module>> modules;
                for (size_t i = 0; i < num; ++i)
                {
                    REQUIRE_NOTHROW (modules.push_back (queue.add (static_cast<int>(rand ()))));
                }
                auto stupdatenum = static_cast<size_t>(rand () % 1024);
                for (size_t i = 0; i < stupdatenum; ++i)
                {
                    size_t modnum = rand () % modules.size ();
                    auto module = modules[modnum];
                    switch (module.lock ()->getStatus ())
                    {
                        case Module::Waiting:
                            queue.setStatus (module, Module::Working);
                            break;
                        case Module::Working:
                            if (rand () % 2)
                            {
                                queue.setStatus (module, Module::Failed);
                            }
                            else
                            {
                                queue.setStatus (module, Module::Completed);
                            }
                            break;
                        default:
                            break;
                    }
                }
                int cleared = 0;
                while (cleared != 15)
                {
                    int statusno;
                    do
                    {
                        statusno = static_cast<uint8_t>(rand () % 4);
                    } while (cleared & (1 << statusno));
                    cleared |= (1 << statusno);
                    Module::Status status;
                    switch (statusno)
                    {
                        case 0:
                            status = Module::Waiting;
                            break;
                        case 1:
                            status = Module::Working;
                            break;
                        case 2:
                            status = Module::Completed;
                            break;
                        case 3:
                            status = Module::Failed;
                            break;
                        default:
                            abort (); //Should not get here
                    }
                    typeof (modules) statmods;
                    for (auto it = modules.begin (); it != modules.end ();)
                    {
                        REQUIRE_FALSE (it->expired ());
                        if (it->lock ()->getStatus () == status)
                        {
                            statmods.push_back (*it);
                            it = modules.erase (it);
                        }
                        else
                        {
                            ++it;
                        }
                    }
                    while (!statmods.empty ())
                    {
                        std::weak_ptr<const Module> module;
                        REQUIRE_NOTHROW (module = queue.first (status));
                        REQUIRE_FALSE (module.expired ());
                        REQUIRE_FALSE (statmods.front ().expired ());
                        REQUIRE (module.lock () == statmods.front ().lock ());
                        REQUIRE_NOTHROW (queue.remove (module));
                        statmods.pop_front ();
                    }
                    REQUIRE (queue.first (status).expired ());
                    for (auto &i : statmods)
                    {
                        REQUIRE (i.expired ());
                    }
                }
                REQUIRE (queue.isEmpty ());
            }
        }
    }

    SECTION("Copy Construct")
    {
        ModuleQueue queue;
        auto num = static_cast<size_t>(rand () % 1024 + 2);
        std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> mods (num);
        for (auto &mod : mods)
        {
            mod.first = static_cast<int>(rand ());
            {
                std::unordered_set<std::string> params;
                size_t paramsnum = static_cast<size_t>(rand ()) % 1024;
                mod.second.reserve (paramsnum);
                for (size_t i = 0; i < paramsnum; ++i)
                {
                    std::string name (randstr ());
                    std::string value (randstr ());
                    if (!name.empty () && params.find (name) == params.end ())
                    {
                        params.insert (name);
                        mod.second.emplace_back (name, value);
                    }
                }
            }
            std::weak_ptr<Module> module;
            REQUIRE_NOTHROW  (queue.add (mod.second, mod.first));
        }
        ModuleQueue *queue2 = nullptr;
        REQUIRE_NOTHROW (queue2 = new ModuleQueue (queue));
        for (auto &i : mods)
        {
            REQUIRE_FALSE (queue.first ().lock () == queue2->first ().lock ());
            REQUIRE (queue2->first ().lock ()->getType () == i.first);
            for (auto &param : i.second)
            {
                REQUIRE (queue2->first ().lock ()->getParam (param.first) == param.second);
            }
            queue2->remove (queue2->first ());
            queue.remove (queue.first ());
        }
        delete queue2;
    }

    SECTION ("Set Param Fail")
    {
        ModuleQueue queue;
        auto type = static_cast<int>(rand ());
        std::vector<std::pair<std::string, std::string>> paramsv;
        {
            std::unordered_set<std::string> params;
            size_t paramsnum = static_cast<size_t>(rand ()) % 1024;
            paramsv.reserve (paramsnum);
            for (size_t i = 0; i < paramsnum; ++i)
            {
                std::string name (randstr ());
                std::string value (randstr ());
                if (!name.empty () && params.find (name) == params.end ())
                {
                    params.insert (name);
                    paramsv.emplace_back (name, value);
                }
            }
        }
        SECTION ("Set Param Expired")
        {

            std::weak_ptr<const Module> module;
            REQUIRE_NOTHROW (module = queue.add (paramsv, type));
            REQUIRE_NOTHROW (queue.remove (module));
            std::string name (randstr ());
            std::string value (randstr ());
            REQUIRE_THROWS_AS (queue.setParam (module, name, value), std::logic_error);
            REQUIRE_THROWS_AS (queue.setParam (std::weak_ptr<const Module> (std::shared_ptr<Module> (nullptr)),
                                               name, value), std::logic_error);
        }
        SECTION ("Set Param Nullptr")
        {
            std::string name (randstr ());
            std::string value (randstr ());
            REQUIRE_THROWS_AS (queue.setParam (std::weak_ptr<const Module> (std::shared_ptr<Module> (nullptr)),
                                               name, value), std::logic_error);
        }
    }

    SECTION ("Set Status")
    {
        ModuleQueue queue;
        SECTION ("Set Status Expired")
        {
            auto type = static_cast<int>(rand ());
            auto module = queue.add (type);
            REQUIRE_NOTHROW (queue.remove (module));
            REQUIRE_THROWS_AS (queue.setStatus (module, Module::Working), std::logic_error);
        }
        SECTION ("Set Status Nullptr")
        {
            REQUIRE_THROWS_AS (queue.setStatus (
                    std::weak_ptr<const Module> (std::shared_ptr<Module> (nullptr)), Module::Working), std::logic_error);
        }
    }





}