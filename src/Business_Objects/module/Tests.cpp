//
// Created by svuatoslav on 10/18/18.
//

#define CATCH_CONFIG_MAIN

#include <catch.hpp>

#include "Module.h"

const char *randstr ()
{
    static char buf[1024];
    size_t len = rand () % (sizeof (buf) / sizeof (char));
    for (size_t i = 0; i < len; i += sizeof (long) / sizeof (char))
    {
        if (len - i >= sizeof (long) / sizeof (char))
        {
            *((long *) (buf + i)) = rand ();
        }
        else
        {
            long rndval = rand ();
            memcpy (buf + i, &rndval, (len - i) * sizeof (char));
        }
    }
    return buf;
}

TEST_CASE ("Module Tests")
{
    srand (static_cast<unsigned int>(time (nullptr)));

    SECTION ("Construct Destruct")
    {
        Module *module = nullptr;
        auto type = static_cast<int>(rand ());

        SECTION("From Type")
        {
            REQUIRE_NOTHROW (module = new Module (type));
            REQUIRE_NOTHROW (delete module);
        }

        SECTION("From Type And Params")
        {
            auto paramnum = static_cast<size_t>(rand () % 1024);
            std::vector<std::pair<std::string, std::string>> params (paramnum);
            for (auto &i : params)
            {
                std::string name (randstr ());
                std::string value (randstr ());
                i = std::make_pair (name, value);
            }
            REQUIRE_NOTHROW (module = new Module (params, type));
            REQUIRE_NOTHROW (delete module);
        }
    }

    SECTION("Get Type")
    {
        SECTION ("Initialized")
        {
            auto type = static_cast<int>(rand ());
            Module module (type);
            REQUIRE (module.getType () == type);
        }

        SECTION ("Uninitialized")
        {
            Module module;
            REQUIRE (module.getType () == 0);
        }

    }

    SECTION("Life Cycle")
    {
        auto type = static_cast<int>(rand ());
        Module module (type);
        SECTION("From Waiting")
        {
            REQUIRE (module.getStatus () == Module::Waiting);
            SECTION ("To Waiting")
            {
                REQUIRE_NOTHROW (module.setStatus (Module::Waiting));
                REQUIRE (module.getStatus () == Module::Waiting);
            }
            SECTION ("To Working")
            {
                REQUIRE_NOTHROW (module.setStatus (Module::Working));
                REQUIRE (module.getStatus () == Module::Working);
            }
            SECTION ("To Completed")
            {
                REQUIRE_THROWS_AS (module.setStatus (Module::Completed), std::logic_error);
                REQUIRE (module.getStatus () == Module::Waiting);
            }
            SECTION ("To Failed")
            {
                REQUIRE_THROWS_AS (module.setStatus (Module::Failed), std::logic_error);
                REQUIRE (module.getStatus () == Module::Waiting);
            }
        }
        SECTION ("From Working")
        {
            REQUIRE_NOTHROW (module.setStatus (Module::Working));
            REQUIRE (module.getStatus () == Module::Working);
            SECTION ("To Waiting")
            {
                REQUIRE_THROWS_AS (module.setStatus (Module::Waiting), std::logic_error);
                REQUIRE (module.getStatus () == Module::Working);
            }
            SECTION ("To Working")
            {
                REQUIRE_NOTHROW (module.setStatus (Module::Working));
                REQUIRE (module.getStatus () == Module::Working);
            }
            SECTION ("To Completed")
            {
                REQUIRE_NOTHROW (module.setStatus (Module::Completed));
                REQUIRE (module.getStatus () == Module::Completed);
            }
            SECTION ("To Failed")
            {
                REQUIRE_NOTHROW (module.setStatus (Module::Failed));
                REQUIRE (module.getStatus () == Module::Failed);
            }
        }
        SECTION ("From Completed")
        {
            REQUIRE_NOTHROW (module.setStatus (Module::Working));
            REQUIRE_NOTHROW (module.setStatus (Module::Completed));
            REQUIRE (module.getStatus () == Module::Completed);
            SECTION ("To Waiting")
            {
                REQUIRE_THROWS_AS (module.setStatus (Module::Waiting), std::logic_error);
                REQUIRE (module.getStatus () == Module::Completed);
            }
            SECTION ("To Working")
            {
                REQUIRE_THROWS_AS (module.setStatus (Module::Working), std::logic_error);
                REQUIRE (module.getStatus () == Module::Completed);
            }
            SECTION ("To Completed")
            {
                REQUIRE_NOTHROW (module.setStatus (Module::Completed));
                REQUIRE (module.getStatus () == Module::Completed);
            }
            SECTION ("To Failed")
            {
                REQUIRE_THROWS_AS (module.setStatus (Module::Failed), std::logic_error);
                REQUIRE (module.getStatus () == Module::Completed);
            }
        }
        SECTION ("From Failed")
        {
            REQUIRE_NOTHROW (module.setStatus (Module::Working));
            REQUIRE_NOTHROW (module.setStatus (Module::Failed));
            REQUIRE (module.getStatus () == Module::Failed);
            SECTION ("To Waiting")
            {
                REQUIRE_THROWS_AS (module.setStatus (Module::Waiting), std::logic_error);
                REQUIRE (module.getStatus () == Module::Failed);
            }
            SECTION ("To Working")
            {
                REQUIRE_THROWS_AS (module.setStatus (Module::Working), std::logic_error);
                REQUIRE (module.getStatus () == Module::Failed);
            }
            SECTION ("To Completed")
            {
                REQUIRE_THROWS_AS (module.setStatus (Module::Completed), std::logic_error);
                REQUIRE (module.getStatus () == Module::Failed);
            }
            SECTION ("To Failed")
            {
                REQUIRE_NOTHROW (module.setStatus (Module::Failed));
                REQUIRE (module.getStatus () == Module::Failed);
            }
        }
    }

    SECTION ("Params")
    {
        auto type = static_cast<int>(rand ());
        auto paramnum = static_cast<size_t>(rand () % 1024);
        std::vector<std::pair<std::string, std::string>> params;
        params.reserve (paramnum);
        std::set<std::string> used;
        for (size_t i = 0; i < paramnum; ++i)
        {
            std::string name (randstr ());
            std::string value (randstr ());
            if (used.find (name) == used.end ())
            {
                params.emplace_back (name, value);
                used.insert (name);
            }
        }

        SECTION("Construct With Params And Get")
        {
            Module module (params, type);
            for (auto &i : params)
            {
                REQUIRE (module.getParam (i.first) == i.second);
            }
        }

        SECTION ("Get Unused Param")
        {
            Module module (type);
            REQUIRE (module.getParam (randstr ()).empty ());
        }

        SECTION ("Set Params")
        {
            Module module (type);
            for (auto &i : params)
            {
                module.setParam (i.first, i.second);
            }
            for (auto &i : params)
            {
                REQUIRE (module.getParam (i.first) == i.second);
            }
        }

        SECTION ("Reset Params")
        {
            Module module (params, type);
            for (auto &i : params)
            {
                std::string val(randstr ());
                REQUIRE_NOTHROW (module.setParam (i.first, val));
                REQUIRE (module.getParam (i.first) == val);
            }
        }

        SECTION ("Remove Params")
        {
            Module module (params, type);
            for (auto &i : params)
            {
                std::string val(randstr ());
                REQUIRE_NOTHROW (module.setParam (i.first, std::string()));
                REQUIRE (module.getParam (i.first).empty ());
            }
        }
    }
}