//
// Created by svuatoslav on 10/18/18.
//

#ifndef PRNG_MANAGER_MODULE_H
#define PRNG_MANAGER_MODULE_H

#include <memory>
#include <unordered_map>
#include <vector>

/*!
 * Class of prng testing process module
 */
class Module
{
public:

    /*!
     * Possible module status enumeration
     */
    enum Status
    {
        Waiting = 0,          ///<Module is waiting to be executed
        Working = 1,          ///<Module is currently being executed
        Completed = 2,        ///<Module has finished executing
        Failed = 3,           ///<Module has failed during execution
    };

private:
    std::unordered_map<std::string, std::string> _parameters;
    Status _status;
    int _type;

public:

    /*!
     * Constructor <br>
     * If input is nullptr, creates an uninitialized object
     * @param type Module type
     */
    explicit Module (int type = 0) noexcept;

    /*!
     * Constructor <br>
     * If input is nullptr, creates an uninitialized object
     * @param params Parameters of the creted module
     * @throws std::bad_alloc
     */
    explicit Module (const std::vector<std::pair<std::string, std::string>> &params, int type = 0);


    /*!
     * Destructor
     */
    ~Module () noexcept = default;


    /*!
     * Set module status
     * @status New status of the module
     * @throws std::logic_error If module can't go to specified state from current state
     */
    void setStatus (Status status);


    /*!
     * Get current module status
     * @return Current module status
     */
    Status getStatus () const noexcept;


    /*!
     * Get module type
     * @return Module type
     */
    int getType () const noexcept;


    /*!
     * Set a module parameter
     * @param name Parameter name
     * @param value Parameter value
     * @throws std::bad_alloc
     */
    void setParam (const std::string &name, const std::string &value);


    /*!
     * Get a module parameter
     * @param name Parameter name
     * @return Parameter value
     */
    const std::string &getParam (const std::string &name) const noexcept;
};


#endif //PRNG_MANAGER_MODULE_H
