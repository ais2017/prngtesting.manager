//
// Created by svuatoslav on 10/18/18.
//

#include "Module.h"

Module::Module (int type) noexcept : _parameters (), _status (Waiting), _type (type)
{
}

Module::Module (const std::vector<std::pair<std::string, std::string>> &params, int type) :
        _parameters (), _status (Waiting), _type (type)
{
    for (auto &i : params)
    {
        setParam (i.first, i.second);
    }
}

const char *statusNames[] = {"Waiting", "Working", "Completed", "Failed"};

void Module::setStatus (Module::Status status)
{
    if (_status == status)
        return;
    bool isNext = true;
    switch (_status)
    {
        case Waiting:
            if (status != Working)
            {
                isNext = false;
            }
            break;
        case Working:
            if (status != Completed && status != Failed)
            {
                isNext = false;
            }
            break;
        default:
            isNext = false;
            break;
    }
    if (isNext)
    {
        _status = status;
    }
    else
    {
        throw std::logic_error (std::string ("Can't move to status ") + statusNames[status] + " from status "
                                + statusNames[_status]);
    }
}

Module::Status Module::getStatus () const noexcept
{
    return _status;
}

int Module::getType () const noexcept
{
    return _type;
}

void Module::setParam (const std::string &name, const std::string &value)
{
    if (value.empty ())
    {
        _parameters.erase (name);
    }
    else
    {
        _parameters[name] = value;
    }
}

const std::string &Module::getParam (const std::string &name) const noexcept
{
    static const std::string emptystr;
    auto it = _parameters.find (name);
    if (it != _parameters.cend ())
    {
        return it->second;
    }
    else
    {
        return emptystr;
    }
}



