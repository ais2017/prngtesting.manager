//
// Created by svuatoslav on 11/18/18.
//

#ifndef PRNG_MANAGER_MANAGERAPP_H
#define PRNG_MANAGER_MANAGERAPP_H

#include <TestStorageInterface.h>
#include <tinyxml.h>
#include "../Manager.h"
#include "frames/MainFrame/ManagerFrame.h"
#include <map>

#include "wx/wx.h"

namespace managerGUI
{
    class ManagerApp : public wxApp, public ControlInterface
    {
    private:

        ManagerFrame *frame;

        std::map<std::string, int> moduleNameToNum;

        std::map<std::string, std::pair<int, std::shared_ptr<ModuleInterface>>> interfaces;

        std::unordered_map<std::string, std::shared_ptr<ModuleInterface>> modNameToIface;

        TestStorageInterface stgi;

        Manager manager;

        std::unordered_map<int, std::shared_ptr<ModuleInterface>> readInterfaces(TiXmlElement *);

        std::vector<std::pair<int, int>> readModuleTypes(TiXmlElement *);

        std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> readModules(TiXmlElement *);

        std::pair<int, std::vector<std::pair<std::string, std::string>>> readSaveModule(TiXmlElement *);

        void OnConf();

        void OnLoad();

        void OnExec();

    public:

        ManagerApp();

        bool OnInit() override;

        void notify(size_t count) override;

    };
}
#endif //PRNG_MANAGER_MANAGERAPP_H
