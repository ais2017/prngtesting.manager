//
// Created by svuatoslav on 11/18/18.
//

#include "ManagerApp.h"
#include "frames/EditModules/SelectConfig.h"
#include "ModuleBuilders/ModuleBuilders.h"

using namespace managerGUI;

void ManagerApp::OnConf()
{
    frame->Disable();
    std::unordered_map<std::string, std::vector<std::string>> ifaceTypeToConfs{};
    std::vector<std::string> ifaceTypes;
    ifaceTypes.reserve(moduleBuilders.size());
    for (auto &i : moduleBuilders)
    {
        auto confs = i.second->configurations;
        confs.emplace_back("name");
        ifaceTypeToConfs.emplace(i.first, confs);
        ifaceTypes.emplace_back(i.first);
    }
    std::unordered_map<std::string, std::vector<std::string>> modTypeToConfs{};
    std::vector<std::string> modTypes;
    modTypes.reserve(modTypes.size());
    for (auto &i : interfaces)
    {
        modTypeToConfs.emplace(i.first, std::vector<std::string>(1, "name"));
        modTypes.emplace_back(i.first);
    }

    SelectConfig fileDialog(frame, _("Select configuration file"),
            {{"Interfaces",   {"Interfaces"},  "Interface", {"name", "type"},      ifaceTypeToConfs, "type",      ifaceTypes},
             {"Module types", {"ModuleTypes"}, "Module",    {"name", "interface"}, modTypeToConfs,   "interface", modTypes}});
    if (fileDialog.ShowModal() == wxID_CANCEL)
    {
        frame->Enable(true);
        return;
    }

    TiXmlDocument doc;
    if (!doc.LoadFile(fileDialog.GetPath().data()))
    {
        wxLogError("Cannot open file '%s':\n'%s'", fileDialog.GetPath(), doc.ErrorDesc());
        frame->Enable(true);
        return;
    }

    TiXmlElement *rootEl = doc.RootElement();
    if (rootEl == nullptr)
    {
        wxLogError("No root element");
        frame->Enable(true);
        return;
    }

    std::unordered_map<int, std::shared_ptr<ModuleInterface>> ifaces;
    try
    {
        ifaces = readInterfaces(rootEl->FirstChildElement("Interfaces"));
    }
    catch (std::exception &ex)
    {
        wxLogError("Failed lo load interfaces:\n%s", ex.what());
        frame->Enable(true);
        return;
    }
    catch (...)
    {
        wxLogError("Failed lo load interfaces due to unknown reason");
        frame->Enable(true);
        return;
    }

    manager = Manager(std::shared_ptr<ControlInterface>(this, [](ControlInterface *) {}),
                      std::shared_ptr<StorageInterface>(&stgi, [](StorageInterface *) {}),
                      ifaces);

    std::vector<std::pair<int, int>> confs;
    try
    {
        confs = readModuleTypes(rootEl->FirstChildElement("ModuleTypes"));
    }
    catch (std::exception &ex)
    {
        wxLogError("Failed lo load module types:\n%s", ex.what());
        frame->Enable(true);
        return;
    }
    catch (...)
    {
        wxLogError("Failed lo load module types due to unknown reason");
        frame->Enable(true);
        return;
    }
    if (!manager.setup(confs))
    {
        wxLogError("Error in configuration file");
        frame->Enable(true);
        return;
    }
    wxMessageBox("Configured successfully");
    frame->Enable(true);
}

void ManagerApp::OnLoad()
{
    std::unordered_map<std::string, std::vector<std::string>> modTypeToConfs{};
    std::vector<std::string> modTypes;
    modTypes.reserve(modTypes.size());
    for (auto &i : moduleNameToNum)
    {
        std::vector<std::string> confs = modNameToIface.at(i.first)->getReqVals();
        confs.emplace_back("tag");
        modTypeToConfs.emplace(i.first, confs);
        modTypes.emplace_back(i.first);
    }
    SelectConfig selectFile(nullptr, "Select",
            {{"Modules",     {"Modules"}, "Module",     {"tag", "type"}, modTypeToConfs, "type", modTypes},
             {"Save module", {},          "SaveModule", {"tag", "type"}, modTypeToConfs, "type", modTypes}});

    if (selectFile.ShowModal() == wxID_CANCEL)
    {
        frame->Enable(true);
        return;
    }

    TiXmlDocument doc;
    if (!doc.LoadFile(selectFile.GetPath().data()))
    {
        wxLogError("Cannot open file '%s':\n'%s'", selectFile.GetPath(), doc.ErrorDesc());
        frame->Enable(true);
        return;
    }
    TiXmlElement *rootEl = doc.RootElement();
    if (rootEl == nullptr)
    {
        wxLogError("No root element");
        frame->Enable(true);
        return;
    }
    std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> mods;
    std::pair<int, std::vector<std::pair<std::string, std::string>>> savemod;
    try
    {
        mods = readModules(rootEl->FirstChildElement("Modules"));
    }
    catch (std::exception &ex)
    {
        wxLogError("Failed lo load modules:\n%s", ex.what());
        frame->Enable(true);
        return;
    }
    catch (...)
    {
        wxLogError("Failed lo load modules due to unknown reason");
        frame->Enable(true);
        return;
    }
    try
    {
        savemod = readSaveModule(rootEl->FirstChildElement("SaveModule"));
    }
    catch (std::exception &ex)
    {
        wxLogError("Failed lo load save module:\n%s", ex.what());
        frame->Enable(true);
        return;
    }
    catch (...)
    {
        wxLogError("Failed lo load save module due to unknown reason");
        frame->Enable(true);
        return;
    }

    if (!manager.setModules(mods, savemod))
    {
        wxLogError("Error in configuration file");
        frame->Enable(true);
        return;
    }
    wxMessageBox("Set modules successfully");
    frame->Enable(true);
}

void ManagerApp::OnExec()
{
    frame->Disable();
    try
    {
        manager.launch();
    }
    catch (std::exception &ex)
    {
        wxLogError("A module finished with an exception:\n%s", ex.what());
        frame->Enable(true);
        return;
    }
    catch (const char *&ex)
    {
        wxLogError("A module finished with a string exception:\n%s", ex);
        frame->Enable(true);
        return;
    }
    catch (std::string &ex)
    {
        wxLogError("A module finished with a string exception:\n%s", ex);
        frame->Enable(true);
        return;
    }
    catch (...)
    {
        wxLogError("A module finished with an unrecognized exception.");
        frame->Enable(true);
        return;
    }
    frame->Enable(true);
    wxMessageDialog dialog(frame, "Save results?", wxMessageBoxCaptionStr, wxYES_NO | wxCENTRE);
    if (dialog.ShowModal() != wxID_YES)
    {
        return;
    }
    try
    {
        manager.save();
    }
    catch (std::exception &ex)
    {
        wxLogError("A module finished with an exception:\n%s", ex.what());
        return;
    }
    catch (const char *&ex)
    {
        wxLogError("A module finished with a string exception:\n%s", ex);
        return;
    }
    catch (std::string &ex)
    {
        wxLogError("A module finished with a string exception:\n%s", ex);
        return;
    }
    catch (...)
    {
        wxLogError("A module finished with an unrecognized exception.");
        return;
    }
    wxMessageBox("Successfully saved.");
}


void ManagerApp::notify(size_t count)
{
    wxMessageBox("Successfully executed " + std::to_string(count) + " modules");
}

std::pair<int, std::vector<std::pair<std::string, std::string>>> ManagerApp::readSaveModule(TiXmlElement *element)
{
    if (element == nullptr)
    {
        wxLogError("No save module found");
        frame->Enable(true);
        throw std::runtime_error("No save module provided");
    }
    const char *savemodbase = element->Attribute("type");
    if (savemodbase == nullptr)
    {
        wxLogError("No type for save module");
        frame->Enable(true);
        throw std::runtime_error("No type for save module provided");
    }
    if (moduleNameToNum.find(savemodbase) == moduleNameToNum.end())
    {
        wxLogError("Undefined type \"%s\" for save module", savemodbase);
        frame->Enable(true);
        throw std::runtime_error("Undefined type of save module");
    }
    std::pair<int, std::vector<std::pair<std::string, std::string>>> savemod;
    savemod.first = moduleNameToNum.at(savemodbase);
    std::vector<std::string> reqparams = modNameToIface.at(savemodbase)->getReqVals();
    std::vector<std::pair<std::string, std::string>> params{};
    for (auto &i : reqparams)
    {
        const char *val = element->Attribute(i.data());
        if (val == nullptr)
        {
            wxLogWarning("No value for parameter \"%s\" in save module.\nAssuming empty string.", i);
            val = "";
        }
        params.emplace_back(i, val);
    }
    savemod.second = std::move(params);
    return savemod;
}

std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>>
ManagerApp::readModules(TiXmlElement *rootEl)
{
    if (rootEl == nullptr)
    {
        wxLogError("Module launch sequence not found");
        throw std::runtime_error("Module launch sequence is not provided");
    }
    std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> mods;
    for (TiXmlElement *element = rootEl->FirstChildElement("Module"); element != nullptr;
         element = element->NextSiblingElement("Module"))
    {
        const char *tag = element->Attribute("tag");
        const char *mod = element->Attribute("type");
        if (mod == nullptr)
        {
            if (tag == nullptr)
            {
                wxLogWarning("No type for tagless module.\nSkipping this module.");
            }
            else
            {
                wxLogWarning("No type for module with tag \"%s\".\nSkipping this module.", tag);
            }
            continue;
        }
        if (moduleNameToNum.find(mod) == moduleNameToNum.end())
        {
            if (tag == nullptr)
            {
                wxLogWarning("Undefined module type \"%s\" in tagless module.\nSkipping this module.", mod);
            }
            else
            {
                wxLogWarning("Undefined module type \"%s\" in module with tag \"%s\".\nSkipping this module.", mod,
                             tag);
            }
            continue;
        }

        std::vector<std::string> reqparams = modNameToIface.at(mod)->getReqVals();
        std::vector<std::pair<std::string, std::string>> params{};
        for (auto &i : reqparams)
        {
            const char *val = element->Attribute(i.data());
            if (val == nullptr)
            {
                if (tag == nullptr)
                {
                    wxLogWarning("No value for parameter \"%s\" in tagless module with type \"%s\".\n"
                                 "Assuming empty string.", i, mod);
                }
                else
                {
                    wxLogWarning("No value for parameter \"%s\" in module with tag \"%s\".\n"
                                 "Assuming empty string.", i, tag);
                }
                val = "";
            }
            params.emplace_back(i, val);
        }
        mods.emplace_back(moduleNameToNum.at(mod), std::move(params));
    }
    return mods;
}

std::unordered_map<int, std::shared_ptr<ModuleInterface>> ManagerApp::readInterfaces(TiXmlElement *rootEl)
{
    if (rootEl == nullptr)
    {
        wxLogError("Module interface types not found");
        throw std::runtime_error("Module interface types are not provided");
    }
    int lastID = 0;
    std::map<std::string, std::pair<int, std::shared_ptr<ModuleInterface>>> ifaces{};
    std::unordered_map<int, std::shared_ptr<ModuleInterface>> ifacesByNum{};
    for (TiXmlElement *element = rootEl->FirstChildElement("Interface"); element != nullptr;
         element = element->NextSiblingElement("Interface"))
    {
        const char *name = element->Attribute("name");
        if (name == nullptr)
        {
            wxLogWarning("No name for an interface.\nIgnoring this interface.");
            continue;
        }
        if (ifaces.find(name) != ifaces.end())
        {
            wxLogWarning("Redifinition of interface \"%s\".\nIgnoring this interface.", name);
            continue;
        }
        const char *type = element->Attribute("type");
        if (type == nullptr)
        {
            wxLogWarning("No type for an interface \"%s\".\nIgnoring this interface.", name);
            continue;
        }
        if (moduleBuilders.find(type) == moduleBuilders.end())
        {
            wxLogWarning("Undefined type \"%s\" for interface \"%s\".\nIgnoring this interface.", type, name);
            continue;
        }
        std::shared_ptr<ModuleInterface> iface;
        try
        {
            iface = moduleBuilders.at(type)->build(element);
        }
        catch (std::exception &ex)
        {
            wxLogWarning("Failed to define interface \"%s\":\n%s\nIgnoring this interface.", name, ex.what());
            continue;
        }
        catch (...)
        {
            wxLogWarning("Failed to define interface \"%s\" due to an unknown reason\nIgnoring this interface.", name);
            continue;
        }
        ifaces.emplace(name, std::make_pair(++lastID, iface));
        ifacesByNum.emplace(lastID, iface);
    }
    interfaces = std::move(ifaces);
    return ifacesByNum;
}

std::vector<std::pair<int, int>> ManagerApp::readModuleTypes(TiXmlElement *rootEl)
{
    if (rootEl == nullptr)
    {
        wxLogError("No module configurations");
        throw std::runtime_error("No module configurations provided");
    }
    int lastID = 0;
    std::vector<std::pair<int, int>> confs{};
    decltype(moduleNameToNum) newModuleNameToNum{};
    decltype(modNameToIface) newModNameToIface{};
    for (TiXmlElement *element = rootEl->FirstChildElement("Module"); element != nullptr;
         element = element->NextSiblingElement("Module"))
    {
        const char *mod = element->Attribute("name");
        if (mod == nullptr)
        {
            wxLogWarning("No name for a module.\nSkipping this module.");
            continue;
        }
        if (newModuleNameToNum.find(mod) != newModuleNameToNum.end())
        {
            wxLogWarning("Module name \"%s\" redifinition.\nSkipping this module.", mod);
            continue;
        }
        const char *iface = element->Attribute("interface");
        if (iface == nullptr)
        {
            wxLogWarning("No interface for module \"%s\".\nSkipping this module.", mod);
            continue;
        }
        if (interfaces.find(iface) == interfaces.end())
        {
            wxLogWarning("No such interface \"%s\" for module \"%s\".\nSkipping this module.", iface, mod);
            continue;
        }
        confs.emplace_back(++lastID, interfaces.at(iface).first);
        newModuleNameToNum[mod] = lastID;
        newModNameToIface[mod] = interfaces[iface].second;
    }
    moduleNameToNum = std::move(newModuleNameToNum);
    modNameToIface = std::move(newModNameToIface);
    return confs;
}

bool ManagerApp::OnInit()
{
    frame = new ManagerFrame("PRNG testing manager",
                             [this]() { this->OnConf(); },
                             [this]() { this->OnLoad(); },
                             [this]() { this->OnExec(); });
    frame->Show(true);
    return true;
}

ManagerApp::ManagerApp() : frame(nullptr), moduleNameToNum(), interfaces(), modNameToIface(), stgi(),
                           manager(std::shared_ptr<ControlInterface>(this, [](ControlInterface *) {}),
                                   std::shared_ptr<StorageInterface>(&stgi, [](StorageInterface *) {}),
                                   {})
{}

IMPLEMENT_APP (ManagerApp);