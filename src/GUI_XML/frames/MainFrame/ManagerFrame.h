//
// Created by svuatoslav on 11/23/18.
//

#ifndef PRNG_MANAGER_MANAGERFRAME_H
#define PRNG_MANAGER_MANAGERFRAME_H

#include <wx/wx.h>
#include <functional>

namespace managerGUI
{
    class ManagerFrame : public wxFrame
    {
        static const int IDConf;
        static const int IDLoad;
        static const int IDExec;

        struct WidgetPos
        {
            wxPoint point;
            wxSize size;
        };

        static const int headerSize;
        static const int border;
        static const int windowSizeX;
        static const int windowSizeY;
        static const int rows;
        static const int columns;

        static const WidgetPos defBtnPos;
        static const WidgetPos confBtnPos;
        static const WidgetPos loadBtnPos;
        static const WidgetPos execBtnPos;

        std::function<void()> _OnConfFunc;

        std::function<void()> _OnLoadFunc;

        std::function<void()> _OnExecFunc;

        void OnConf(wxCommandEvent &event);

        void OnLoad(wxCommandEvent &event);

        void OnExec(wxCommandEvent &event);

    public:
        explicit ManagerFrame(const wxString &title, const std::function<void()> &OnConfFunc,
                              const std::function<void()> &OnLoadFunc,
                              const std::function<void()> &OnExecFunc);
    };
}

#endif //PRNG_MANAGER_MANAGERFRAME_H
