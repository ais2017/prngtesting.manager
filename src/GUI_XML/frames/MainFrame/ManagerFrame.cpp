//
// Created by svuatoslav on 11/23/18.
//

#include "ManagerFrame.h"

using namespace managerGUI;

const int ManagerFrame::IDConf = 101;
const int ManagerFrame::IDLoad = 102;
const int ManagerFrame::IDExec = 103;

const int ManagerFrame::headerSize = 30;
const int ManagerFrame::border = 20;
const int ManagerFrame::windowSizeX = 300;
const int ManagerFrame::windowSizeY = 300;
const int ManagerFrame::rows = 3;
const int ManagerFrame::columns = 1;

const ManagerFrame::WidgetPos ManagerFrame::defBtnPos = {{border, border},
                                                         {(windowSizeX - border * 2) / columns,
                                                          (windowSizeY - border * 2 - headerSize) / rows}};
const ManagerFrame::WidgetPos ManagerFrame::confBtnPos = defBtnPos;
const ManagerFrame::WidgetPos ManagerFrame::loadBtnPos = {{defBtnPos.point.x, confBtnPos.point.y + confBtnPos.size.y},
                                                          defBtnPos.size};
const ManagerFrame::WidgetPos ManagerFrame::execBtnPos = {{defBtnPos.point.x, loadBtnPos.point.y + loadBtnPos.size.y},
                                                          defBtnPos.size};


ManagerFrame::ManagerFrame(const wxString &title, const std::function<void()> &OnConfFunc,
                           const std::function<void()> &OnLoadFunc,
                           const std::function<void()> &OnExecFunc) :
        wxFrame(nullptr, wxID_ANY, title, wxDefaultPosition, wxSize(windowSizeX, windowSizeY)),
        _OnConfFunc(OnConfFunc), _OnLoadFunc(OnLoadFunc), _OnExecFunc(OnExecFunc)
{

    wxPanel *panel = new wxPanel(this, wxID_ANY);

    wxButton *loadConfBtn = new wxButton(panel, IDConf, wxT("Configure"),   confBtnPos.point, confBtnPos.size);
    wxButton *loadModsBtn = new wxButton(panel, IDLoad, wxT("Set modules"), loadBtnPos.point, loadBtnPos.size);
    wxButton *execModsBtn = new wxButton(panel, IDExec, wxT("Launch"),      execBtnPos.point, execBtnPos.size);

    Connect(IDConf, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ManagerFrame::OnConf));
    Connect(IDLoad, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ManagerFrame::OnLoad));
    Connect(IDExec, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ManagerFrame::OnExec));

    loadConfBtn->SetFocus();
    loadModsBtn->SetFocus();
    execModsBtn->SetFocus();

    SetMinSize(GetSize());
    SetMaxSize(GetSize());

    Centre();
}

void ManagerFrame::OnConf(wxCommandEvent &)
{
    _OnConfFunc();
}

void ManagerFrame::OnLoad(wxCommandEvent &)
{
    _OnLoadFunc();
}

void ManagerFrame::OnExec(wxCommandEvent &)
{
    _OnExecFunc();
}
