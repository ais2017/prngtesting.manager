//
// Created by svuatoslav on 11/23/18.
//

#ifndef PRNG_MANAGER_EDISSEQUENCEPANEL_H
#define PRNG_MANAGER_EDISSEQUENCEPANEL_H

#include <wx/wx.h>
#include <wx/listctrl.h>
#include <vector>
#include <tinyxml.h>
#include <tinyxml.h>
#include <unordered_map>

namespace managerGUI
{
    class EditSequencePanel : public wxPanel
    {
        static const int IDTitle;
        static const int IDList;
        static const int IDUp;
        static const int IDDown;
        static const int IDEdit;
        static const int IDNew;
        static const int IDDelete;

        struct WidgetPos
        {
            wxPoint point;
            wxSize size;
        };

    public:

        static const int panelSizeX;
        static const int panelSizeY;

    private:

        static const int titleHeight;
        static const int listHeight;

        static const WidgetPos defPos;
        static const WidgetPos titlePos;
        static const WidgetPos listPos;
        static const WidgetPos upBtnPos;
        static const WidgetPos downBtnPos;
        static const WidgetPos editBtnPos;
        static const WidgetPos newBtnPos;
        static const WidgetPos deleteBtnPos;

        wxListCtrl *list;

        TiXmlElement *_group;
        std::string _name;
        std::vector<std::string> _display;

        std::unordered_map<std::string, std::vector<std::string>> _typeToAttributes;
        std::string _typeAttribute;
        std::vector<std::string> _types;

    public:

        EditSequencePanel(wxWindow *parent, const std::string &title, wxPoint pos, TiXmlElement *group,
                          const std::string &name,
                          const std::vector<std::string> &display,
                          const std::unordered_map<std::string, std::vector<std::string>> &typeToAttributes,
                          const std::string &typeAttribute, const std::vector<std::string> &types);

        bool edit(TiXmlElement *);

        void edit(wxCommandEvent &event);

        void newentry(wxCommandEvent &event);

        void deleteentry(wxCommandEvent &event);

        void onUp(wxCommandEvent &event);

        void onDown(wxCommandEvent &event);

    };
}

#endif //PRNG_MANAGER_EDISSEQUENCEPANEL_H
