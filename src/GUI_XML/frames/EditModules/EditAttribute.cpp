//
// Created by Святослав on 24.11.2018.
//

#include "EditAttribute.h"

using namespace managerGUI;

const int EditAttribute::IDAttribute = 101;
const int EditAttribute::IDValue = 102;
const int EditAttribute::IDSelect = 103;

const int EditAttribute::TextDivBtn = 3;

inline int EditAttribute::textLen(int sumLen)
{
    return (sumLen * TextDivBtn) / (2 * TextDivBtn + 1);
}

EditAttribute::EditAttribute(wxWindow *parent, const wxPoint &pos, const wxSize &size, const std::string &name,
                             TiXmlElement *element) : wxPanel(parent, wxID_ANY, pos, size), _name(name),
                                                      _element(element)
{
    _attribute = new wxStaticText(this, IDAttribute, name, {0, 0}, {textLen(size.x), size.y});

    const char *value = nullptr;
    if (element != nullptr)
    {
        value = element->Attribute(name.data());
    }
    if (value == nullptr)
    {
        value = "";
    }

    _value = new wxTextCtrl(this, IDValue, value, {textLen(size.x), 0}, {textLen(size.x), size.y});
    _select = new wxButton(this, IDSelect, "File", {2 * textLen(size.x), 0},
                           {size.x - 2 * textLen(size.x), size.y});

    Connect(IDSelect, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditAttribute::selectFile));
}

void EditAttribute::selectFile(wxCommandEvent &)
{
    wxFileDialog fileDialog(this, "Select file", "", "", "All files (*)|*", wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    if (fileDialog.ShowModal() == wxID_CANCEL)
    {
        return;
    }
    _value->SetValue(fileDialog.GetPath());
}

bool EditAttribute::apply() const
{
    if (_element == nullptr)
    {
        return false;
    }
    wxString val = _value->GetValue();
    if (strcmp(val, "") == 0)
    {
        _element->RemoveAttribute(_name.data());
    }
    else
    {
        _element->SetAttribute(_name.data(), val.c_str());
    }
    return true;
}
