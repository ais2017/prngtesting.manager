//
// Created by svuatoslav on 11/23/18.
//

#include <tuple>
#include "EditSequence.h"
#include "EditSequencePanel.h"

using namespace managerGUI;

const int EditSequence::headerSize = 30;
const int EditSequence::border = 20;

EditSequence::EditSequence(wxWindow *parent, const wxString &title, TiXmlElement *root,
                           const std::vector<std::tuple<std::string, std::vector<std::string>, std::string,
                                   std::vector<std::string>, std::unordered_map<std::string, std::vector<std::string>>,
                                   std::string, std::vector<std::string>>> &groups) :
        wxDialog(parent, wxID_ANY, title, wxDefaultPosition,
                 wxSize(EditSequencePanel::panelSizeX * groups.size() + 2 * border,
                        EditSequencePanel::panelSizeY + 2 * border + headerSize))
{
    wxPanel *mainPanel = new wxPanel(this);
    if (root == nullptr)
    {
        wxLogError("Nullptr passed as xml document root");
        return;
    }
    for (size_t i = 0; i < groups.size(); ++i)
    {
        TiXmlElement *element = root;
        for (auto &elName : std::get<1>(groups[i]))
        {
            TiXmlElement *newElement = element->FirstChildElement(elName.data());
            if (newElement == nullptr)
            {
                element->InsertEndChild(TiXmlElement(elName.data()));
                newElement = element->FirstChildElement(elName.data());
                if (newElement == nullptr)
                {
                    element = nullptr;
                    break;
                }
            }
            element = newElement;
        }
        new EditSequencePanel(mainPanel, std::get<0>(groups[i]),
                              wxPoint(border + EditSequencePanel::panelSizeX * i, border),
                              element, std::get<2>(groups[i]), std::get<3>(groups[i]), std::get<4>(groups[i]),
                              std::get<5>(groups[i]),
                              std::get<6>(groups[i]));
    }
    SetMinSize(GetSize());
    SetMaxSize(GetSize());
    Centre();
}
