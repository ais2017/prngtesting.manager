//
// Created by svuatoslav on 11/23/18.
//

#ifndef PRNG_MANAGER_SELECTFILE_H
#define PRNG_MANAGER_SELECTFILE_H

#include <wx/wx.h>
#include <tinyxml.h>
#include <vector>
#include <tuple>
#include <unordered_map>

namespace managerGUI
{
    class SelectConfig : public wxDialog
    {

        static const int IDFilename;
        static const int IDSelect;
        static const int IDLoad;
        static const int IDEdit;

        struct WidgetPos
        {
            wxPoint point;
            wxSize size;
        };

        static const int headerSize;
        static const int border;
        static const int windowSizeX;
        static const int windowSizeY;

        static const int selectBtnLen;

        static const WidgetPos defPos;
        static const WidgetPos filenamePos;
        static const WidgetPos selectBtnPos;
        static const WidgetPos loadBtnPos;
        static const WidgetPos editBtnPos;

        std::string path;

        wxTextCtrl *filename;

        std::vector<std::tuple<
                std::string,               //Title
                std::vector<std::string>,  //Group path
                std::string,               //Element name
                std::vector<std::string>,  //Values to display
                std::unordered_map<std::string, std::vector<std::string>>, //Type to req attributes
                std::string,               //Type attribute name
                std::vector<std::string>   //Allowed types
        >> _groups;

    public:

        SelectConfig(wxWindow *parent, const wxString &title, const std::vector<std::tuple<
                std::string,               //Title
                std::vector<std::string>,  //Group path
                std::string,               //Element name
                std::vector<std::string>,  //Values to display
                std::unordered_map<std::string, std::vector<std::string>>, //Type to req attributes
                std::string,               //Type attribute name
                std::vector<std::string>   //Allowed types
        >> &groups = {});

        std::string GetPath();

        void selectFile(wxCommandEvent &event);

        void load(wxCommandEvent &event);

        void edit(wxCommandEvent &event);

    };
}

#endif //PRNG_MANAGER_SELECTFILE_H
