//
// Created by svuatoslav on 11/23/18.
//

#ifndef PRNG_MANAGER_EDITMODULE_H
#define PRNG_MANAGER_EDITMODULE_H

#include <wx/wx.h>
#include <tinyxml.h>
#include <unordered_map>
#include <vector>
#include "EditAttribute.h"

namespace managerGUI
{
    class EditElement : public wxDialog
    {

        static const int IDApply;
        static const int IDCancel;
        static const int IDChoice;

        struct WidgetPos
        {
            wxPoint point;
            wxSize size;
        };

    public:

        static const int windowSizeX;
        static const int windowSizeY;

    private:

        static const int headerSize;
        static const int border;
        static const int scrollbarReserve;

        static const int fieldHeight;

        static const WidgetPos defPos;
        static const WidgetPos choicePos;
        static const WidgetPos scrolledPos;
        static const WidgetPos applyPos;
        static const WidgetPos cancelPos;

        wxPanel *_panel;
        wxChoice *_choice;
        wxScrolledWindow *_scrolled;

        std::vector<EditAttribute *> editors;

        TiXmlElement *_element;

        std::unordered_map<std::string, std::vector<std::string>> _typeToAttributes;

        std::string _typeAttribute;

        std::vector<std::string> _types;

    public:

        explicit EditElement(wxWindow *parent, TiXmlElement *element,
                             const decltype(_typeToAttributes) &typeToAttributes, const std::string &typeAttribute,
                             const std::vector<std::string> &types);

        void apply(wxCommandEvent &);

        void cancel(wxCommandEvent &);

        void changeType(wxCommandEvent &);
    };
}

#endif //PRNG_MANAGER_EDITMODULE_H
