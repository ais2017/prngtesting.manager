//
// Created by svuatoslav on 11/23/18.
//

#include "SelectConfig.h"
#include "EditSequence.h"

using namespace managerGUI;

const int SelectConfig::IDFilename = 201;
const int SelectConfig::IDSelect = 202;
const int SelectConfig::IDLoad = 203;
const int SelectConfig::IDEdit = 204;

const int SelectConfig::headerSize = 20;
const int SelectConfig::border = 20;
const int SelectConfig::windowSizeX = 300;
const int SelectConfig::windowSizeY = 150;

const int SelectConfig::selectBtnLen = 60;

const SelectConfig::WidgetPos SelectConfig::defPos = {{border, border},
                                                      {(windowSizeX - border * 2) / 2,
                                                               (windowSizeY - border * 2 - headerSize) / 2}};
const SelectConfig::WidgetPos SelectConfig::filenamePos = {defPos.point,
                                                           {windowSizeX - 2 * border - selectBtnLen, defPos.size.y}};
const SelectConfig::WidgetPos SelectConfig::selectBtnPos = {{filenamePos.point.x +
                                                             filenamePos.size.x, filenamePos.point.y},
                                                            {selectBtnLen,       filenamePos.size.y}};
const SelectConfig::WidgetPos SelectConfig::loadBtnPos = {
        {filenamePos.point.x, filenamePos.point.y + filenamePos.size.y},
        defPos.size};
const SelectConfig::WidgetPos SelectConfig::editBtnPos = {{loadBtnPos.point.x + loadBtnPos.size.x, loadBtnPos.point.y},
                                                          defPos.size};


std::string SelectConfig::GetPath()
{
    return path;
}

SelectConfig::SelectConfig(wxWindow *parent, const wxString &title,
                           const std::vector<std::tuple<std::string, std::vector<std::string>, std::string,
                                   std::vector<std::string>, std::unordered_map<std::string, std::vector<std::string>>,
                                   std::string, std::vector<std::string>>> &groups) :
        wxDialog(parent, wxID_ANY, title, wxDefaultPosition, wxSize(windowSizeX, windowSizeY)),
        _groups(groups)
{
    wxPanel *panel = new wxPanel(this, wxID_ANY);

    filename = new wxTextCtrl(panel, IDFilename, wxT("file_name"), filenamePos.point, filenamePos.size);
    wxButton *selectBtn = new wxButton(panel, IDSelect, wxT("Select"), selectBtnPos.point, selectBtnPos.size);
    wxButton *loadBtn = new wxButton(panel, IDLoad, wxT("Load"), loadBtnPos.point, loadBtnPos.size);
    wxButton *editBtn = new wxButton(panel, IDEdit, wxT("Edit"), editBtnPos.point, editBtnPos.size);

    Connect(IDSelect, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(SelectConfig::selectFile));
    Connect(IDLoad, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(SelectConfig::load));
    Connect(IDEdit, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(SelectConfig::edit));

    filename->SetFocus();
    selectBtn->SetFocus();
    loadBtn->SetFocus();
    editBtn->SetFocus();

    if (groups.empty())
    {
        editBtn->Disable();
    }

    SetMinSize(GetSize());
    SetMaxSize(GetSize());

    Centre();

}

void SelectConfig::selectFile(wxCommandEvent &)
{
    wxFileDialog fileDialog(this, "Select configuration file", "", "", "xml files (*.xml)|*.xml|All files (*)|*",
                            wxFD_OPEN | wxFD_FILE_MUST_EXIST);
    if (fileDialog.ShowModal() == wxID_CANCEL)
    {
        return;
    }
    filename->SetValue(fileDialog.GetPath());
}

void SelectConfig::load(wxCommandEvent &)
{
    path = filename->GetValue().c_str();
    EndModal(wxID_OK);
}

void SelectConfig::edit(wxCommandEvent &)
{
    TiXmlDocument doc;
    if (!doc.LoadFile(filename->GetValue().c_str()))
    {
        if (doc.ErrorId() == TiXmlBase::TIXML_ERROR_DOCUMENT_EMPTY)
        {
            doc.InsertEndChild(TiXmlElement("ConfigurationRoot"));
            if (!doc.SaveFile(filename->GetValue().c_str()))
            {
                wxLogError("Failed to initialize xml file:\n%s", doc.ErrorDesc());
                return;
            }
        }
        else
        {
            wxLogError("Failed to open xml file:\n%s", doc.ErrorDesc());
            return;
        }
    }
    TiXmlElement *root = doc.RootElement();
    if (root == nullptr)
    {
        doc.InsertEndChild(TiXmlElement("ConfigurationRoot"));
        root = doc.RootElement();
        if (root == nullptr)
        {
            wxLogError("Root element was not found in xml file and could not be created.");
            return;
        }

    }
    EditSequence editdialog(this, "Edit sequence", root, _groups);
    editdialog.ShowModal();
    doc.SaveFile(filename->GetValue().c_str());
}