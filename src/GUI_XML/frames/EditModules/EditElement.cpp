//
// Created by svuatoslav on 11/23/18.
//

#include "EditElement.h"

using namespace managerGUI;

const int EditElement::IDApply = 101;
const int EditElement::IDCancel = 102;
const int EditElement::IDChoice = 103;


const int EditElement::headerSize = 20;
const int EditElement::border = 30;

const int EditElement::windowSizeX = 500;
const int EditElement::windowSizeY = 500;

const int EditElement::fieldHeight = 30;
const int EditElement::scrollbarReserve = 20;

const EditElement::WidgetPos EditElement::defPos = {{border,                         border},
                                                    {(windowSizeX - border * 2) / 2, fieldHeight}};
const EditElement::WidgetPos EditElement::choicePos = {defPos.point, {defPos.size.x * 2, defPos.size.y}};
const EditElement::WidgetPos EditElement::scrolledPos = {{choicePos.point.x, choicePos.point.y + choicePos.size.y},
                                                         {choicePos.size.x,
                                                          windowSizeY - 2 * border - headerSize - defPos.size.y * 2}};
const EditElement::WidgetPos EditElement::applyPos = {{scrolledPos.point.x, scrolledPos.point.y + scrolledPos.size.y},
                                                      defPos.size};
const EditElement::WidgetPos EditElement::cancelPos = {{applyPos.point.x + applyPos.size.x, applyPos.point.y},
                                                       defPos.size};

EditElement::EditElement(wxWindow *parent, TiXmlElement *element,
                         const decltype(_typeToAttributes) &typeToAttributes, const std::string &typeAttribute,
                         const std::vector<std::string> &types) :
        wxDialog(parent, wxID_ANY, "Edit element", wxDefaultPosition, {windowSizeX, windowSizeY}),
        _element(element), _typeToAttributes(typeToAttributes), _typeAttribute(typeAttribute), _types(types)
{
    _panel = new wxPanel(this);

    const char *type = nullptr;

    if (element != nullptr)
    {
        type = element->Attribute(typeAttribute.data());
    }

    int currentSelection = -1;
    std::vector<wxString> choices;
    choices.reserve(_types.size());
    for (size_t i = 0; i < _types.size(); ++i)
    {
        choices.emplace_back(types[i]);
        if (type != nullptr && types[i] == type)
        {
            currentSelection = i;
        }
    }

    _choice = new wxChoice(_panel, IDChoice, choicePos.point, choicePos.size, choices.size(), choices.data());

    if (currentSelection >= 0)
    {
        _choice->SetSelection(currentSelection);
    }

    _scrolled = new wxScrolledWindow(_panel, wxID_ANY, scrolledPos.point, scrolledPos.size);

    if (element != nullptr && currentSelection >= 0 && type != nullptr)
    {
        std::vector<std::string> attributes = typeToAttributes.at(type);
        editors.reserve(attributes.size());
        wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
        for (size_t i = 0; i < attributes.size(); ++i)
        {
            EditAttribute *editAttribute = new EditAttribute(_scrolled, {0, static_cast<int>(i) * fieldHeight},
                                                             {scrolledPos.size.x - scrollbarReserve, fieldHeight},
                                                             attributes[i], element);
            sizer->Add(editAttribute);
            editors.push_back(editAttribute);
        }
        _scrolled->SetSizer(sizer);
        _scrolled->FitInside();
        _scrolled->SetScrollRate(5, 5);
    }

    new wxButton(_panel, IDApply, "Apply", applyPos.point, applyPos.size);
    new wxButton(_panel, IDCancel, "Cancel", cancelPos.point, cancelPos.size);

    Connect(IDApply, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditElement::apply));
    Connect(IDCancel, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditElement::cancel));
    Connect(IDChoice, wxEVT_CHOICE, wxCommandEventHandler(EditElement::changeType));

}

void EditElement::apply(wxCommandEvent &)
{
    if (_element != nullptr)
    {
        int typechoice = _choice->GetSelection();
        if (typechoice != wxNOT_FOUND)
        {
            std::string type = _types.at(typechoice);
            _element->SetAttribute(_typeAttribute.data(), type.data());
        }
        for (auto &i : editors)
        {
            i->apply();
        }
    }
    EndModal(wxID_OK);
}

void EditElement::cancel(wxCommandEvent &)
{
    EndModal(wxID_CANCEL);
}

void EditElement::changeType(wxCommandEvent &)
{
    int typechoice = _choice->GetSelection();
    if (typechoice != wxNOT_FOUND)
    {
        std::string type = _types.at(typechoice);
        std::vector<std::string> attributes = _typeToAttributes.at(type);
        for (auto &i : editors)
        {
            _scrolled->RemoveChild(i);
            delete i;
        }
        editors.clear();
        editors.reserve(attributes.size());
        wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
        for (size_t i = 0; i < attributes.size(); ++i)
        {
            EditAttribute *editAttribute = new EditAttribute(_scrolled, {0, static_cast<int>(i) * fieldHeight},
                                                             {scrolledPos.size.x - scrollbarReserve, fieldHeight},
                                                             attributes[i], _element);
            editors.push_back(editAttribute);
            sizer->Add(editAttribute);
        }
        _scrolled->SetSizer(sizer);
        _scrolled->FitInside();
        _scrolled->SetScrollRate(5, 5);
    }
}
