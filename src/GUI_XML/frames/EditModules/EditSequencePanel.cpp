//
// Created by svuatoslav on 11/23/18.
//

#include "EditSequencePanel.h"
#include "EditElement.h"

using namespace managerGUI;



const int EditSequencePanel::IDTitle = 300;
const int EditSequencePanel::IDList = 301;
const int EditSequencePanel::IDUp = 302;
const int EditSequencePanel::IDDown = 303;
const int EditSequencePanel::IDEdit = 304;
const int EditSequencePanel::IDNew = 305;
const int EditSequencePanel::IDDelete = 306;

const int EditSequencePanel::panelSizeX = 300;
const int EditSequencePanel::panelSizeY = 500;

const int EditSequencePanel::titleHeight = 30;
const int EditSequencePanel::listHeight = 300;

const EditSequencePanel::WidgetPos EditSequencePanel::defPos = {{0,            0},
                                                                {(panelSizeX), (panelSizeY - listHeight - titleHeight) /
                                                                               4}};
const EditSequencePanel::WidgetPos EditSequencePanel::titlePos = {defPos.point, {panelSizeX, titleHeight}};
const EditSequencePanel::WidgetPos EditSequencePanel::listPos = {{titlePos.point.x, titlePos.point.y + titlePos.size.y},
                                                                 {panelSizeX,       listHeight}};
const EditSequencePanel::WidgetPos EditSequencePanel::upBtnPos = {{listPos.point.x,   listPos.point.y + listPos.size.y},
                                                                  {defPos.size.x / 2, defPos.size.y}};
const EditSequencePanel::WidgetPos EditSequencePanel::downBtnPos = {
        {upBtnPos.point.x + upBtnPos.size.x, upBtnPos.point.y},
        upBtnPos.size};
const EditSequencePanel::WidgetPos EditSequencePanel::editBtnPos = {
        {upBtnPos.point.x, upBtnPos.point.y + upBtnPos.size.y},
        defPos.size};
const EditSequencePanel::WidgetPos EditSequencePanel::newBtnPos = {
        {editBtnPos.point.x, editBtnPos.point.y + editBtnPos.size.y},
        defPos.size};
const EditSequencePanel::WidgetPos EditSequencePanel::deleteBtnPos = {
        {newBtnPos.point.x, newBtnPos.point.y + newBtnPos.size.y},
        defPos.size};

EditSequencePanel::EditSequencePanel(wxWindow *parent, const std::string &title, wxPoint pos, TiXmlElement *group,
                                     const std::string &name,
                                     const std::vector<std::string> &display,
                                     const std::unordered_map<std::string, std::vector<std::string>> &typeToAttributes,
                                     const std::string &typeAttribute, const std::vector<std::string> &types)
        : wxPanel(parent, wxID_ANY, pos, wxSize(panelSizeX, panelSizeY)), _group(group),
          _name(name), _display(display), _typeToAttributes(typeToAttributes),
          _typeAttribute(typeAttribute), _types(types)
{
    new wxStaticText(this, IDTitle, title, titlePos.point, titlePos.size);
    list = new wxListCtrl(this, IDList, listPos.point, listPos.size, wxLC_REPORT);
    wxButton *upBtn = new wxButton(this, IDUp, wxT("Up"), upBtnPos.point, upBtnPos.size);
    wxButton *downBtn = new wxButton(this, IDDown, wxT("Down"), downBtnPos.point, downBtnPos.size);
    wxButton *editBtn = new wxButton(this, IDEdit, wxT("Edit"), editBtnPos.point, editBtnPos.size);
    wxButton *newBtn = new wxButton(this, IDNew, wxT("Add"), newBtnPos.point, newBtnPos.size);
    wxButton *deleteBtn = new wxButton(this, IDDelete, wxT("Delete"), deleteBtnPos.point, deleteBtnPos.size);

    Connect(IDUp, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditSequencePanel::onUp));
    Connect(IDDown, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditSequencePanel::onDown));
    Connect(IDEdit, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditSequencePanel::edit));
    Connect(IDNew, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditSequencePanel::newentry));
    Connect(IDDelete, wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(EditSequencePanel::deleteentry));

    list->SetFocus();
    upBtn->SetFocus();
    downBtn->SetFocus();
    editBtn->SetFocus();
    newBtn->SetFocus();
    deleteBtn->SetFocus();

    SetMinSize(GetSize());
    SetMaxSize(GetSize());

    for (int i = 0; i < static_cast<int>(display.size()); ++i)
    {
        wxListItem col;
        col.SetId(i);
        col.SetText(display[i]);
        col.SetWidth(static_cast<int>(panelSizeX / display.size()));
        list->InsertColumn(i, col);
    }

    if (group != nullptr)
    {
        int count = 0;
        for (TiXmlElement *val = group->FirstChildElement(name.data()); val != nullptr;
             val = val->NextSiblingElement(name.data()))
        {
            wxListItem item;
            item.SetId(count++);
            item.SetText("");
            list->InsertItem(item);
        }

        count = 0;
        for (TiXmlElement *val = group->FirstChildElement(name.data()); val != nullptr;
             val = val->NextSiblingElement(name.data()))
        {
            for (size_t i = 0; i < display.size(); ++i)
            {
                const char *value = val->Attribute(display[i].data());
                list->SetItem(count, i, value == nullptr ? "" : value);
            }
            ++count;
        }
    }
}

void EditSequencePanel::edit(wxCommandEvent &)
{
    long selection = list->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
    if (selection != wxNOT_FOUND)
    {
        TiXmlElement *selectedEl = _group->FirstChildElement(_name.data());
        for (int i = 0; i < selection && selectedEl != nullptr;
             ++i, selectedEl = selectedEl->NextSiblingElement(_name.data()))
        {}
        if (selectedEl == nullptr)
        {
            return;
        }
        if (edit(selectedEl))
        {
            for (size_t i = 0; i < _display.size(); ++i)
            {
                const char *value = selectedEl->Attribute(_display[i].data());
                list->SetItem(selection, i, value == nullptr ? "" : value);
            }
        }
    }
}

void EditSequencePanel::newentry(wxCommandEvent &)
{
    TiXmlElement element(_name.data());
    if (edit(&element))
    {
        _group->InsertEndChild(element);
        wxListItem item;
        item.SetId(list->GetItemCount());
        item.SetText("");
        list->InsertItem(item);
        for (size_t i = 0; i < _display.size(); ++i)
        {
            const char *value = element.Attribute(_display[i].data());
            list->SetItem(list->GetItemCount() - 1, i, value == nullptr ? "" : value);
        }
    }
}

void EditSequencePanel::deleteentry(wxCommandEvent &)
{
    long selection = list->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
    if (selection != wxNOT_FOUND)
    {
        TiXmlElement *selectedEl = _group->FirstChildElement(_name.data());
        for (int i = 0; i < selection && selectedEl != nullptr;
             ++i, selectedEl = selectedEl->NextSiblingElement(_name.data()))
        {}
        if (selectedEl == nullptr)
        {
            return;
        }
        _group->RemoveChild(selectedEl);
        for (int nextSelection = list->GetNextItem(selection); nextSelection != wxNOT_FOUND;
             selection = nextSelection, nextSelection = list->GetNextItem(selection))
        {
            wxListItem item;
            item.SetId(nextSelection);
            list->GetItem(item);
            item.SetId(selection);
            list->SetItem(item);
            for (int i = 0; i < list->GetColumnCount(); ++i)
            {
                list->SetItem(selection, i, list->GetItemText(nextSelection, i));
            }
        }
        for (int i = 0; i < list->GetColumnCount(); ++i)
        {
            list->SetItem(selection, i, "");
        }
        list->Update();
        list->DeleteItem(selection);
    }
}

void EditSequencePanel::onUp(wxCommandEvent &)
{
    long selection = list->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
    if (selection != wxNOT_FOUND && selection != 0)
    {
        wxListItem item;
        wxListItem prevItem;
        item.SetId(selection);
        prevItem.SetId(selection - 1);
        if (list->GetItem(item) && list->GetItem(prevItem))
        {
            TiXmlElement *prevEl = _group->FirstChildElement(_name.data());
            for (int i = 0; i < selection - 1 && prevEl != nullptr;
                 ++i, prevEl = prevEl->NextSiblingElement(_name.data()))
            {}
            if (prevEl == nullptr)
            {
                return;
            }
            TiXmlElement *selectedEl = prevEl->NextSiblingElement(_name.data());
            if (selectedEl == nullptr)
            {
                return;
            }
            TiXmlElement element(*prevEl);
            _group->ReplaceChild(prevEl, *selectedEl);
            _group->ReplaceChild(selectedEl, element);

            item.SetId(selection - 1);
            prevItem.SetId(selection);
            for (int i = 0; i < list->GetColumnCount(); ++i)
            {
                wxString str = list->GetItemText(selection - 1, i);
                list->SetItem(selection - 1, i, list->GetItemText(selection, i));
                list->SetItem(selection, i, str);
            }
            list->SetItemState(selection - 1, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);
            list->SetItemState(selection, 0, wxLIST_STATE_SELECTED | wxLIST_STATE_FOCUSED);
        }
    }
}

void EditSequencePanel::onDown(wxCommandEvent &)
{
    long selection = list->GetNextItem(-1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED);
    if (selection != wxNOT_FOUND && selection != list->GetItemCount() - 1)
    {
        wxListItem item;
        wxListItem nextItem;
        item.SetId(selection);
        nextItem.SetId(selection + 1);
        if (list->GetItem(item) && list->GetItem(nextItem))
        {
            TiXmlElement *selectedEl = _group->FirstChildElement(_name.data());
            for (int i = 0; i < selection && selectedEl != nullptr;
                 ++i, selectedEl = selectedEl->NextSiblingElement(_name.data()))
            {}
            if (selectedEl == nullptr)
            {
                return;
            }
            TiXmlElement *nextEl = selectedEl->NextSiblingElement(_name.data());
            if (nextEl == nullptr)
            {
                return;
            }
            TiXmlElement element(*selectedEl);
            _group->ReplaceChild(selectedEl, *nextEl);
            _group->ReplaceChild(nextEl, element);

            for (int i = 0; i < list->GetColumnCount(); ++i)
            {
                wxString str = list->GetItemText(selection, i);
                list->SetItem(selection, i, list->GetItemText(selection + 1, i));
                list->SetItem(selection + 1, i, str);
            }
            list->SetItemState(selection + 1, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);
            list->SetItemState(selection, 0, wxLIST_STATE_SELECTED | wxLIST_STATE_FOCUSED);
        }
    }
}

bool EditSequencePanel::edit(TiXmlElement *element)
{
    if (element == nullptr)
    {
        return false;
    }
    EditElement editor(this, element, _typeToAttributes, _typeAttribute, _types);
    editor.ShowModal();
    return true;
}

