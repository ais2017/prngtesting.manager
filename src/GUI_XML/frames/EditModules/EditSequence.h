//
// Created by svuatoslav on 11/23/18.
//

#ifndef PRNG_MANAGER_EDITSEQUENCE_H
#define PRNG_MANAGER_EDITSEQUENCE_H


#include <wx/wx.h>
#include <tinyxml.h>
#include <vector>
#include <unordered_map>

namespace managerGUI
{
    class EditSequence : public wxDialog
    {
        static const int headerSize;
        static const int border;

    public:

        EditSequence(wxWindow *parent, const wxString &title, TiXmlElement *root = nullptr,
                     const std::vector<std::tuple<
                             std::string,               //Title
                             std::vector<std::string>,  //Group path
                             std::string,               //Element name
                             std::vector<std::string>,  //Values to display
                             std::unordered_map<std::string, std::vector<std::string>>, //Type to req attributes
                             std::string,               //Type attribute name
                             std::vector<std::string>   //Allowed types
                     >> &groups = {});

    };
}

#endif //PRNG_MANAGER_EDITSEQUENCE_H
