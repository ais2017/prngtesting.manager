//
// Created by Святослав on 24.11.2018.
//

#ifndef PRNG_MANAGER_EDITATTRIBUTE_H
#define PRNG_MANAGER_EDITATTRIBUTE_H


#include <tinyxml.h>
#include "wx/wx.h"

namespace managerGUI
{
    class EditAttribute : public wxPanel
    {
        const int static IDAttribute;
        const int static IDValue;
        const int static IDSelect;

        const int static TextDivBtn;

        std::string _name;
        TiXmlElement *_element;

        wxStaticText *_attribute;
        wxTextCtrl *_value;
        wxButton *_select;

        inline static int textLen(int sumLen);

    public:

        EditAttribute(wxWindow *parent, const wxPoint &pos, const wxSize &size, const std::string &name,
                      TiXmlElement *element);

        void selectFile(wxCommandEvent &);

        bool apply() const;

    };
}

#endif //PRNG_MANAGER_EDITATTRIBUTE_H
