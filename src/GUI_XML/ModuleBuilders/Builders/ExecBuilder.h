//
// Created by svuatoslav on 11/24/18.
//

#ifndef PRNG_MANAGER_EXECBUILDER_H
#define PRNG_MANAGER_EXECBUILDER_H

#include "../ModuleBuilders.h"
#include <Executable/ExecIface.h>

template<>
class ModuleBuilder<ExecIface> : public BaseModuleBuilder
{
public:
    ModuleBuilder();

    std::shared_ptr<ModuleInterface> build(const TiXmlElement *element) override;
};


#endif //PRNG_MANAGER_EXECBUILDER_H
