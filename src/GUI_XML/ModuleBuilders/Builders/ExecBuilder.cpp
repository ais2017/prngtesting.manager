//
// Created by svuatoslav on 11/24/18.
//

#include "ExecBuilder.h"

ModuleBuilder<ExecIface>::ModuleBuilder() : BaseModuleBuilder({"executable"})
{}

std::shared_ptr<ModuleInterface> ModuleBuilder<ExecIface>::build(const TiXmlElement *element)
{
    if (element == nullptr)
    {
        throw std::invalid_argument("Passed nullptr as executable xml element.");
    }
    const char *executable = element->Attribute("executable");
    if (executable == nullptr)
    {
        throw std::invalid_argument("No executable path provided for executable module");
    }
    return std::make_shared<ExecIface>(executable);
}