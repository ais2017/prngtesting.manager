//
// Created by svuatoslav on 11/24/18.
//

#include "KSBuilder.h"

ModuleBuilder<KSIface>::ModuleBuilder() : BaseModuleBuilder({"executable"})
{};

std::shared_ptr<ModuleInterface> ModuleBuilder<KSIface>::build(const TiXmlElement *element)
{
    if (element == nullptr)
    {
        throw std::invalid_argument("Passed nullptr as Kolmogorov-Smirnov xml element.");
    }
    const char *executable = element->Attribute("executable");
    if (executable == nullptr)
    {
        throw std::invalid_argument("No executable path provided for Kolmogorov-Smirnov module");
    }
    return std::make_shared<KSIface>(executable);
}
