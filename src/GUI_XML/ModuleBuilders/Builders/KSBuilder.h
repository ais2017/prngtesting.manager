//
// Created by svuatoslav on 11/25/18.
//

#ifndef PRNG_MANAGER_KSBUILDER_H
#define PRNG_MANAGER_KSBUILDER_H

#include "ExecBuilder.h"
#include <KolmogorovSmirnov/KSIface.h>

template<>
class ModuleBuilder<KSIface> : public BaseModuleBuilder
{
public:
    ModuleBuilder();

    std::shared_ptr<ModuleInterface> build(const TiXmlElement *element) override;
};

#endif //PRNG_MANAGER_KSBUILDER_H
