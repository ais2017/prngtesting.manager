//
// Created by malaschuk on 11/29/18.
//

#ifndef PRNG_MANAGER_PARSEBUILDER_H
#define PRNG_MANAGER_PARSEBUILDER_H

#include "ExecBuilder.h"
#include <Parser/ParseIface.h>

template<>
class ModuleBuilder<ParseIface> : public BaseModuleBuilder
{
public:
    ModuleBuilder();

    std::shared_ptr<ModuleInterface> build(const TiXmlElement *element) override;
};

#endif //PRNG_MANAGER_PARSEBUILDER_H
