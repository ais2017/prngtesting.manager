//
// Created by svuatoslav on 12/3/18.
//

#include "DBSaverBuilder.h"
#include <unordered_set>

ModuleBuilder<DBSaver>::ModuleBuilder() :
                BaseModuleBuilder({"Host", "Port", "DBName", "Username", "Password",
                                   "ConfigFile", "SequenceFile"})
{
}

std::shared_ptr<ModuleInterface> ModuleBuilder<DBSaver>::build(const TiXmlElement *element)
{
    std::string host;
    int port;
    std::string dbname;
    std::string username;
    std::string password;
    std::vector<std::tuple<std::string, std::string, std::vector<std::string>>> files;
    std::string confname;

    const char* attribute;

    attribute = element->Attribute("ConfigFile");
    if(attribute == nullptr)
    {
        throw std::invalid_argument("No config file specified");
    }

    TiXmlDocument config;
    if (!config.LoadFile(attribute))
    {
        throw std::runtime_error("Cannot open config file '%s':\n'%s'" + std::string(attribute) + config.ErrorDesc());
    }

    TiXmlElement *confel = config.RootElement();
    if(confel == nullptr)
    {
        throw std::runtime_error("No root in config xml file");
    }
    std::unordered_map<std::string, std::vector<std::string>> fileparams{};
    for(confel = confel->FirstChildElement("Module"); confel != nullptr;
        confel = confel->NextSiblingElement("Module"))
    {
        attribute = confel->Attribute("moduleType");
        if(attribute == nullptr)
        {
            continue;
        }
        std::string type = attribute;
        std::vector<std::string> params;
        for(const TiXmlElement * param = confel->FirstChildElement("File"); param != nullptr;
            param = param->NextSiblingElement("File"))
        {
            attribute = param->Attribute("param");
            if(attribute != nullptr)
            {
                params.emplace_back(attribute);
            }
        }
        fileparams.emplace(type, params);
    }
    if(!fileparams.empty())
    {
        attribute = element->Attribute("SequenceFile");
        if(attribute == nullptr)
        {
            throw std::invalid_argument("No launch sequence file specified");
        }
        TiXmlDocument seqdoc;
        if (!seqdoc.LoadFile(attribute))
        {
            throw std::runtime_error("Cannot open launch sequence file '%s':\n'%s'" + std::string(attribute) +
                seqdoc.ErrorDesc());
        }
        const TiXmlElement *tmpel = seqdoc.RootElement();
        if(tmpel == nullptr)
        {
            throw std::runtime_error("Unrecognized module sequence file structure");
        }
        tmpel = tmpel->FirstChildElement("Modules");
        if(tmpel == nullptr)
        {
            throw std::runtime_error("Unrecognized module sequence file structure");
        }
        for(const TiXmlElement * mod = tmpel->FirstChildElement("Module"); mod != nullptr;
            mod = mod -> NextSiblingElement("Module"))
        {
            attribute =  mod->Attribute("type");
            if(attribute != nullptr)
            {
                std::string type = attribute;
                std::string tag;
                attribute = mod->Attribute("tag");
                if (attribute != nullptr)
                {
                    tag = attribute;
                }
                if (fileparams.find(type) != fileparams.cend())
                {
                    std::vector<std::string> filenames;
                    for (auto &i : fileparams[type])
                    {
                        attribute = mod->Attribute(i.data());
                        if (attribute != nullptr)
                        {
                            filenames.emplace_back(attribute);
                        }
                    }
                    files.emplace_back(type, tag, filenames);
                }
            }
        }
    }

    attribute = element->Attribute("Host");
    if(attribute != nullptr)
    {
        host = attribute;
    }
    else
    {
        host = "127.0.0.1";
    }

    attribute = element->Attribute("Port");
    if(attribute != nullptr)
    {
        port = std::stoi(attribute);
        if(port < 0 || port >= 65536)
        {
            throw std::invalid_argument(std::string("Invalid port: ") + attribute + "( " + std::to_string(port) + " )");
        }
    }
    else
    {
        port = 5432;
    }

    attribute = element->Attribute("DBName");
    if(attribute != nullptr)
    {
        dbname = attribute;
    }
    else
    {
        dbname = "prngtests";
    }

    attribute = element->Attribute("Username");
    if(attribute != nullptr)
    {
        username = attribute;
    }
    else
    {
        username = "postgres";
    }

    attribute = element->Attribute("Password");
    if(attribute != nullptr)
    {
        password = attribute;
    }

    return std::make_shared<DBSaver>(host, port, dbname, username, password, files);
}
