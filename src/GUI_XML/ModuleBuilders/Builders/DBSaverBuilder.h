//
// Created by svuatoslav on 12/3/18.
//

#ifndef PRNG_MANAGER_DBSAVEBUILDER_H
#define PRNG_MANAGER_DBSAVEBUILDER_H

#include "../ModuleBuilders.h"
#include <DBSaver/DBSaver.h>

template<>
class ModuleBuilder<DBSaver>  : public BaseModuleBuilder
{
public:
    ModuleBuilder();

    std::shared_ptr<ModuleInterface> build(const TiXmlElement *element) override;
};


#endif //PRNG_MANAGER_DBSAVEBUILDER_H
