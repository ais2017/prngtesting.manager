//
// Created by malaschuk on 11/29/18.
//

#include "ParseBuilder.h"

ModuleBuilder<ParseIface>::ModuleBuilder() : BaseModuleBuilder({"executable"})
{};

std::shared_ptr<ModuleInterface> ModuleBuilder<ParseIface>::build(const TiXmlElement *element)
{
    if (element == nullptr)
    {
        throw std::invalid_argument("Passed nullptr as parser xml element.");
    }
    const char *executable = element->Attribute("executable");
    if (executable == nullptr)
    {
        throw std::invalid_argument("No executable path provided for parser module");
    }
    return std::make_shared<ParseIface>(executable);
}
