//
// Created by svuatoslav on 11/24/18.
//

#include "ModuleBuilders.h"

#include <TestModuleInterface.h>
#include "Builders/ExecBuilder.h"
#include "Builders/KSBuilder.h"
#include "Builders/DBSaverBuilder.h"
#include "Builders/ParseBuilder.h"

const std::map<std::string, std::unique_ptr<BaseModuleBuilder>> moduleBuilders([]()
{
    std::map<std::string, std::unique_ptr<BaseModuleBuilder>> ret{};
    //Add your module interfaces builders here
    ret.emplace("Test",       new ModuleBuilder<TestModuleInterface>);
    ret.emplace("Executable", new ModuleBuilder<ExecIface>);
    ret.emplace("KS",         new ModuleBuilder<KSIface>);
    ret.emplace("DBSaver",    new ModuleBuilder<DBSaver>);
    ret.emplace("Parser",     std::make_unique<ModuleBuilder<ParseIface>>());
    return ret;
}());
