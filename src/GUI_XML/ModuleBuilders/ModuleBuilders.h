#include <utility>

//
// Created by svuatoslav on 11/24/18.
//

#ifndef PRNG_MANAGER_BUILERLOADER_H
#define PRNG_MANAGER_BUILERLOADER_H

#include "ModuleInterface.h"
#include <tinyxml.h>
#include <map>

class BaseModuleBuilder
{
public:
    const std::vector<std::string> configurations;

    explicit BaseModuleBuilder(const std::vector<std::string> &configurations = {}) : configurations(configurations)
    {};

    virtual std::shared_ptr<ModuleInterface> build(const TiXmlElement *element) = 0;
};

extern const std::map<std::string, std::unique_ptr<BaseModuleBuilder>> moduleBuilders;

template<class T>
class ModuleBuilder : public BaseModuleBuilder
{
public:
    typename std::enable_if<std::is_base_of<ModuleInterface, T>::value
                            and not std::is_abstract<T>::value>::type check() = delete;

    explicit ModuleBuilder(const std::vector<std::string> &configurations = {}) : BaseModuleBuilder(configurations)
    {};

    std::shared_ptr<ModuleInterface> build(const TiXmlElement *) override
    { return std::make_shared<T>(); };
};

#endif //PRNG_MANAGER_BUILERLOADER_H
