cmake_minimum_required(VERSION 3.7)
project(prng.manager)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-Wall -Wextra")

add_subdirectory(Builders)

add_library(ModuleBuilders ModuleBuilders.h ModuleBuilders.cpp)

#Link your builders here
target_link_libraries(ModuleBuilders ExecutableInterfaceBuilder KSInterfaceBuilder DBSaverBuilder ParserInterfaceBuilder)