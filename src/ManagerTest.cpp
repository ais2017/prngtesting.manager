//
// Created by svuatoslav on 11/8/18.
//

#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <TestControlInterface.h>
#include <TestStorageInterface.h>
#include <TestModuleInterface.h>
#include "Manager.h"

TEST_CASE ("Manager Tests")
{
    srand (time (nullptr) );
    const size_t ifacenum = 100;
    std::shared_ptr<TestControlInterface> ctli = std::make_shared<TestControlInterface> ();
    std::shared_ptr<TestStorageInterface> stgi = std::make_shared<TestStorageInterface> ();
    std::unordered_map<int, std::shared_ptr<TestModuleInterface>> realmodi;
    std::unordered_map<int, std::shared_ptr<ModuleInterface>> modi;
    for(size_t i = 1; i <= ifacenum; ++i)
    {
        realmodi.emplace (i, std::make_shared<TestModuleInterface>());
    }
    for(size_t i = 1; i <= ifacenum; ++i)
    {
        modi.emplace (i, realmodi[i]);
    }

    SECTION ("Construct And Destruct")
    {
        Manager *manager;
        REQUIRE_NOTHROW (manager = new Manager(ctli, stgi, modi));
        REQUIRE_NOTHROW (delete manager);
    }

    SECTION ("Config")
    {
        Manager manager(ctli, stgi, modi);
        SECTION ("Empty Config")
        {
            REQUIRE (manager.setup (std::vector<std::pair<int, int>>()));
            REQUIRE (stgi->getModuleSettings ().empty ());
        }

        SECTION ("Correct Config")
        {
            std::vector<std::pair<int, int>> confs;
            size_t num = static_cast<size_t>(rand () % 100);
            for(size_t i = 0; i < num; ++i)
            {
                confs.emplace_back (i + 1, rand() % 99 + 1);
            }
            REQUIRE (manager.setup (confs));
            REQUIRE (stgi->getModuleSettings () == confs);
        }

        SECTION ("Incorrect Config")
        {
            SECTION ("Bad Type")
            {

                SECTION ("Bad Module Type")
                {
                    std::vector<std::pair<int, int>> confs;
                    size_t num1 = static_cast<size_t>(rand () % 100);
                    for(size_t i = 0; i < num1; ++i)
                    {
                        confs.emplace_back (i + 1, rand() % 99 + 1);
                    }
                    confs.emplace_back (-(rand ()%1024), rand() % 99 + 1);
                    size_t num2 = static_cast<size_t>(rand () % 100);
                    for(size_t i = 0; i < num2; ++i)
                    {
                        confs.emplace_back (i + num1 + 1, rand() % 99 + 1);
                    }
                    REQUIRE_FALSE (manager.setup (confs));
                    REQUIRE (stgi->getModuleSettings ().empty());
                }

                SECTION ("Bad Interface Type")
                {
                    std::vector<std::pair<int, int>> confs;
                    size_t num1 = static_cast<size_t>(rand () % 100);
                    size_t num2 = static_cast<size_t>(rand () % 100);
                    for(size_t i = 0; i < num1; ++i)
                    {
                        confs.emplace_back (i + 1, rand() % 99 + 1);
                    }
                    confs.emplace_back (num1 + num2 + 2, -(rand() % 1024 ));
                    for(size_t i = 0; i < num2; ++i)
                    {
                        confs.emplace_back (i + num1 + 1, rand() % 99 + 1);
                    }
                    REQUIRE_FALSE (manager.setup (confs));
                    REQUIRE (stgi->getModuleSettings ().empty());
                }
            }

            SECTION ("Duplicate Type")
            {
                std::vector<std::pair<int, int>> confs;
                size_t num1 = static_cast<size_t>(rand () % 100);
                size_t num2 = static_cast<size_t>(rand () % 100);
                for(size_t i = 0; i < num1; ++i)
                {
                    confs.emplace_back (i + 1, rand() % 99 + 1);
                }
                confs.emplace_back (rand() % (num1 + num2) + 1, rand() % 99 + 1);
                for(size_t i = 0; i < num2; ++i)
                {
                    confs.emplace_back (i + num1 + 1, rand() % 99 + 1);
                }
                REQUIRE_FALSE (manager.setup (confs));
                REQUIRE (stgi->getModuleSettings ().empty());
            }

            SECTION ("Wrong Interface Type")
            {
                std::vector<std::pair<int, int>> confs;
                size_t num1 = static_cast<size_t>(rand () % 100);
                size_t num2 = static_cast<size_t>(rand () % 100);
                for(size_t i = 0; i < num1; ++i)
                {
                    confs.emplace_back (i + 1, rand() % 99 + 1);
                }
                confs.emplace_back (num1 + num2 + 2, rand() % 99 + 101 );
                for(size_t i = 0; i < num2; ++i)
                {
                    confs.emplace_back (i + num1 + 1, rand() % 99 + 1);
                }
                REQUIRE_FALSE (manager.setup (confs));
                REQUIRE (stgi->getModuleSettings ().empty());
            }

        }
    }

    SECTION ("Set Modules")
    {
        Manager manager(ctli, stgi, modi);
        std::vector<std::pair<int, int>> confs;
        size_t num = static_cast<size_t>(rand () % 100);
        for(size_t i = 0; i < num; ++i)
        {
            confs.emplace_back (i + 1, rand() % 99 + 1);
        }
        REQUIRE(manager.setup(confs));

        std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> mods;
        std::pair<int, std::vector<std::pair<std::string, std::string>>> saveMod;

        SECTION ("No Modules")
        {
            saveMod.first = static_cast<int>(rand () % num + 1);
            REQUIRE (manager.setModules (mods, saveMod));
            REQUIRE (stgi->getModules ().empty());
            REQUIRE (stgi->getSaveModule () == saveMod);
        }

        SECTION ("Correct Modules")
        {
            saveMod.first = static_cast<int>(rand () % num + 1);
            size_t modnum = rand() % num;
            mods.reserve(modnum);
            for(size_t i = 0; i < modnum; ++i)
            {
                mods.emplace_back (i + 1, std::vector<std::pair<std::string, std::string>>());
            }
            REQUIRE (manager.setModules(mods, saveMod));
            REQUIRE (stgi->getModules () == mods);
            REQUIRE (stgi->getSaveModule () == saveMod);
        }

        SECTION ("Incorrect Modules")
        {
            SECTION ("Wrong Module Type")
            {
                SECTION ("Regular Module")
                {
                    saveMod.first = static_cast<int>(rand () % num + 1);
                    size_t modnum = rand() % (num - 1) + 1;
                    mods.reserve(modnum);
                    for(size_t i = 0; i < modnum; ++i)
                    {
                        mods.emplace_back (i + 1, std::vector<std::pair<std::string, std::string>>());
                    }
                    mods[rand() % modnum].first += num;
                    REQUIRE_FALSE (manager.setModules (mods, saveMod));
                    REQUIRE (stgi->getModules ().empty ());
                }

                SECTION ("Save Module")
                {
                    saveMod.first = static_cast<int>(rand () % num + 1 + num);
                    size_t modnum = rand() % num;
                    mods.reserve(modnum);
                    for(size_t i = 0; i < modnum; ++i)
                    {
                        mods.emplace_back (i + 1, std::vector<std::pair<std::string, std::string>>());
                    }
                    REQUIRE_FALSE (manager.setModules (mods, saveMod));
                    REQUIRE (stgi->getModules ().empty ());
                }
            }
        }
    }

    SECTION ("Launch")
    {
        Manager manager(ctli, stgi, modi);
        std::vector<std::pair<int, int>> confs;
        for(size_t i = 0; i < ifacenum; ++i)
        {
            confs.emplace_back (i + 1, i + 1);
        }
        stgi->writeModulesConfig (confs);
        std::vector<std::pair<std::string, std::string>> savemodparam;
        int saveval = static_cast<int>(rand());
        savemodparam.emplace_back ("TestVal", std::to_string (saveval));
        stgi->writeSaveModule (std::make_pair (1, savemodparam));
        SECTION ("No Modules")
        {
            REQUIRE_NOTHROW (manager.launch());
            for(size_t i = 1; i <= ifacenum; ++i)
            {
                REQUIRE (realmodi[i]->getTested().empty());
            }
            REQUIRE (ctli->getNotifications () == 1);
        }

        SECTION ("With Modules")
        {
            std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> mods;
            for(size_t i = 1; i < ifacenum; ++i)
            {
                std::vector<std::pair<std::string, std::string>> params;
                params.emplace_back ("TestVal", std::to_string (i + 1));
                mods.emplace_back (i + 1, params);
            }
            stgi->writeModules (mods);

            SECTION ("Correct")
            {
                REQUIRE_NOTHROW (manager.launch ());
                REQUIRE (realmodi[1]->getTested().empty());
                for(size_t i = 2; i <= ifacenum; ++i)
                {
                    REQUIRE (realmodi[i]->getTested().size() == 1);
                    REQUIRE (realmodi[i]->getTested()[0] == i);
                }
                REQUIRE (ctli->getNotifications () == 1);
            }

            SECTION ("Corrupted Storage")
            {

                SECTION ("Bad Config")
                {
                    auto conf = stgi->getModuleSettings ();
                    conf[rand()%ifacenum].first *= -1;
                    stgi->writeModulesConfig (conf);
                    REQUIRE_THROWS_AS (manager.launch (), std::runtime_error);
                    for(size_t i = 1; i <= ifacenum; ++i)
                    {
                        REQUIRE (realmodi[i]->getTested().empty());
                    }
                    REQUIRE (ctli->getNotifications () == 0);
                }

                SECTION ("Bad Modules")
                {
                    auto mods = stgi->getModules ();
                    mods[rand()%ifacenum].first *= -1;
                    stgi->writeModules (mods);
                    REQUIRE_THROWS_AS (manager.launch (), std::runtime_error);
                    for(size_t i = 1; i <= ifacenum; ++i)
                    {
                        REQUIRE (realmodi[i]->getTested().empty());
                    }
                    REQUIRE (ctli->getNotifications () == 0);
                }
            }

            SECTION ("Execution Failure")
            {
                auto mods = stgi->getModules ();
                size_t badMod = rand()%(ifacenum - 1);
                mods[badMod].second.emplace_back ("WannaThrow?", "YesPlease!");
                stgi->writeModules (mods);
                REQUIRE_THROWS_AS (manager.launch (), std::invalid_argument);
                REQUIRE (realmodi[1]->getTested().empty());
                badMod = mods[badMod].first;
                for(size_t i = 2; i <= badMod; ++i)
                {
                    REQUIRE (realmodi[i]->getTested().size() == 1);
                    REQUIRE (realmodi[i]->getTested()[0] == i);
                }
                for(size_t i = badMod + 1; i <= ifacenum; ++i)
                {
                    REQUIRE (realmodi[i]->getTested().empty());
                }
                REQUIRE (ctli->getNotifications () == 0);

//                SECTION ("Save Module")
//                {
//                    auto mod = stgi->getSaveModule ();
//                    mod.second.emplace_back ("WannaThrow?", "YesPlease!");
//                    stgi->writeSaveModule (mod);
//                    TestControlInterface::Params params;
//                    params.op = ControlInterface::operation::Launch;
//                    ctli->addParam (params);
//                    params.save = true;
//                    ctli->addParam (params);
//                    params.op = ControlInterface::operation::Quit;
//                    ctli->addParam (params);
//                    REQUIRE_NOTHROW (manager.work ());
//                    REQUIRE (ctli->nextNotification () == ControlInterface::Success);
//                    REQUIRE (ctli->nextNotification () == ControlInterface::ExecutionFailure);
//                    REQUIRE (ctli->nextNotification () == ControlInterface::Success);
//                    REQUIRE (realmodi[1]->getTested().size() == 1);
//                    REQUIRE (realmodi[1]->getTested()[0] == saveval);
//                    for(size_t i = 2; i <= ifacenum; ++i)
//                    {
//                        REQUIRE (realmodi[i]->getTested().size() == 1);
//                        REQUIRE (realmodi[i]->getTested()[0] == i);
//                    }
//                }

            }

        }

    }

    SECTION ("Save")
    {
        Manager manager(ctli, stgi, modi);
        std::vector<std::pair<int, int>> confs;
        for(size_t i = 0; i < ifacenum; ++i)
        {
            confs.emplace_back (i + 1, i + 1);
        }
        stgi->writeModulesConfig (confs);

        SECTION("Success")
        {
            std::vector<std::pair<std::string, std::string>> savemodparam;
            int saveval = static_cast<int>(rand());
            savemodparam.emplace_back ("TestVal", std::to_string (saveval));
            stgi->writeSaveModule (std::make_pair (1, savemodparam));

            REQUIRE_NOTHROW (manager.save());
            REQUIRE (realmodi[1]->getTested().size() == 1);
            REQUIRE (realmodi[1]->getTested()[0] == saveval);
        }

        SECTION("Error")
        {
            std::vector<std::pair<std::string, std::string>> savemodparam;
            int saveval = static_cast<int>(rand());
            savemodparam.emplace_back ("TestVal", std::to_string (saveval));
            savemodparam.emplace_back ("WannaThrow?", "YesPlease!");
            stgi->writeSaveModule (std::make_pair (1, savemodparam));

            REQUIRE_THROWS_AS (manager.save(), std::invalid_argument);
            REQUIRE (realmodi[1]->getTested().size() == 1);
            REQUIRE (realmodi[1]->getTested()[0] == saveval);
        }

    }

}