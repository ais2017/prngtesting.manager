//
// Created by svuatoslav on 11/4/18.
//

#include "Manager.h"
#include "Control_Interface/ControlInterface.h"
#include <unordered_set>
#include <TestModuleInterface.h>

Manager::Manager (const std::shared_ptr<ControlInterface> &ctli, const std::shared_ptr<StorageInterface> &stgi,
                  const std::unordered_map<int, std::shared_ptr<ModuleInterface>> &ifaces):
                  ctli(ctli), stgi(stgi), ifaces(ifaces)
{
}

bool Manager::setup (const std::vector<std::pair<int, int>> &config)
{
        if(checkConfig (config))
        {
            stgi->writeModulesConfig (config);
            return true;
        }
        else
        {
            return false;
        }
}

bool Manager::setModules (const std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> &modulesData,
                          const std::pair<int, std::vector<std::pair<std::string, std::string>>> &saveModule)
{
    auto confs = stgi->getModuleSettings ();
    if (checkModules (confs, modulesData, saveModule))
    {
        stgi->writeModules (modulesData);
        stgi->writeSaveModule (saveModule);
        return true;
    }
    return false;
}

void Manager::launch ()
{
    auto conf = stgi->getModuleSettings ();
    auto mods = stgi->getModules ();
    auto saveMod = stgi->getSaveModule ();
    if(!checkAll (conf, mods, saveMod))
    {
        throw std::runtime_error("Wrong configurations");
    }

    size_t count = mods.size ();
    ModuleQueue queue;
    {
        for (auto &i : mods)
        {
            queue.add (i.second, i.first);
        }
    }
    std::unordered_map<int, std::shared_ptr<ModuleInterface>> moduleIfaces;
    {
        for(auto & i : conf)
        {
            if(ifaces.find (i.second) != ifaces.end ())
            {
                moduleIfaces.emplace (i.first, ifaces[i.second]);
            }
        }
    }
    while (!queue.isEmpty ())
    {
        auto mod = queue.first ();
        auto reqparams = moduleIfaces.at(mod.lock()->getType ())->getReqVals();
        std::unordered_map<std::string, std::string> params;
        for(auto &i : reqparams)
        {
            params.emplace (i, mod.lock ()->getParam (i));
        }
        moduleIfaces.at (mod.lock ()->getType ())->execute (params);
        queue.remove (mod);
    }
    ctli->notify (count);
}

bool Manager::checkModules (const std::vector<std::pair<int, int>> &config,
               const std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> &modulesData,
               const std::pair<int, std::vector<std::pair<std::string, std::string>>> &saveModule) const
{
    std::unordered_set<int> declmods;
    for(auto &i : config)
    {
        declmods.insert (i.first);
    }
    for(auto & i : modulesData)
    {
        if(declmods.find (i.first) == declmods.end ())
        {
            return false;
        }
    }
    if(declmods.find (saveModule.first) == declmods.end ())
    {
        return false;
    }
    return true;
}

bool Manager::checkConfig (const std::vector<std::pair<int, int>> &conf) const
{
    std::unordered_set<int> declared;
    for(auto & i : conf)
    {
        if(i.first <= 0 || i.second <= 0)
        {
            return false;
        }
        if(declared.find(i.first) != declared.end ())
        {
            return false;
        }
        if (ifaces.find (i.second) == ifaces.end ())
        {
            return false;
        }
        declared.insert (i.first);
    }
    return true;
}

bool Manager::checkAll (const std::vector<std::pair<int, int>> &config,
               const std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> &modulesData,
               const std::pair<int, std::vector<std::pair<std::string, std::string>>> &saveModule) const
{
    return checkConfig (config) && checkModules (config, modulesData, saveModule);
}

void Manager::save ()
{
    auto conf = stgi->getModuleSettings ();
    std::unordered_map<int, std::shared_ptr<ModuleInterface>> moduleIfaces;
    {
        for(auto & i : conf)
        {
            if(ifaces.find (i.second) != ifaces.end ())
            {
                moduleIfaces.emplace (i.first, ifaces[i.second]);
            }
        }
    }
    auto saveMod = stgi->getSaveModule ();
    auto reqparams = moduleIfaces.at(saveMod.first)->getReqVals();
    std::unordered_map<std::string, std::string> realparams;
    for(auto &i : saveMod.second)
    {
        realparams.emplace (i.first, i.second);
    }
    std::unordered_map<std::string, std::string> params;
    for(auto &i : reqparams)
    {
        if(realparams.find (i) == realparams.end ())
        {
            params.emplace (i, "");
        }
        else
        {
            params.emplace (i, realparams.at(i));
        }
    }
    moduleIfaces.at (saveMod.first)->execute (params);
}
