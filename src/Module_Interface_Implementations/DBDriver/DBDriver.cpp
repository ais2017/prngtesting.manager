#include <libpq-fe.h>
#include <cstring>
#include "DBDriver.h"

#define _TESTTABLENAME   "test"
#define _MODULETABLENAME "module"
#define _RESULTTABLENAME "result"

#define _TESTID          "test_id"
#define _TESTTIME        "submission_time"

#define _MODULEID        "module_id"
#define _MODULETYPE      "type"
#define _MODULETAG       "tag"

#define _RESULTID        "result_id"
#define _FILENAME        "file_name"
#define _RESULTDATA      "result_data"

static const char * tables[] =          {_TESTTABLENAME,
                                         _MODULETABLENAME,
                                         _RESULTTABLENAME};

static const char * testcolumns[] =     {_TESTID,
                                         _TESTTIME};

static const char * modulecolumns[] =   {_MODULEID,
                                         _TESTID,
                                         _MODULETYPE,
                                         _MODULETAG};

static const char * resultcolumns[] =   {_RESULTID,
                                         _MODULEID,
                                         _FILENAME,
                                         _RESULTDATA};

static const char createtesttable[] =   "CREATE TABLE " _TESTTABLENAME " ("
                                        _TESTID          " SERIAL PRIMARY KEY,"
                                        _TESTTIME        " TIMESTAMP DEFAULT CURRENT_TIMESTAMP"
                                        " );";

static const char createmoduletable[] = "CREATE TABLE " _MODULETABLENAME " ("
                                        _MODULEID      " SERIAL PRIMARY KEY,"
                                        _TESTID        " INT REFERENCES " _TESTTABLENAME "(" _TESTID "),"
                                        _MODULETYPE    " name,"
                                        _MODULETAG     " name"
                                        " );";

static const char createresulttable[] = "CREATE TABLE " _RESULTTABLENAME " ("
                                        _RESULTID      " SERIAL PRIMARY KEY,"
                                        _MODULEID      " INT REFERENCES " _MODULETABLENAME "(" _MODULEID "),"
                                        _FILENAME      " VARCHAR,"
                                        _RESULTDATA    " bytea"
                                        " );";

static const char tableExistsQuery[] =  "SELECT EXISTS ("
                                        " SELECT 1"
                                        " FROM   information_schema.tables"
                                        " WHERE  table_schema = 'public'"
                                        " AND    table_name = $1"
                                        " );";

static const char tableGetColumns[] =   "SELECT column_name"
                                        " FROM information_schema.columns"
                                        " WHERE table_schema = 'public'"
                                        " AND table_name   = $1;";

static const char inserttest[] = "INSERT INTO " _TESTTABLENAME " default values RETURNING " _TESTID ", " _TESTTIME ";";

static const char insertmodule[] =  "INSERT INTO " _MODULETABLENAME " VALUES (DEFAULT, $1, $2, $3) "
                                    "RETURNING " _MODULEID ", " _TESTID ", " _MODULETYPE ", " _MODULETAG ";";

static const char insertresult[] =  "INSERT INTO " _RESULTTABLENAME " VALUES (DEFAULT, $1, $2, $3) "
                                    "RETURNING " _RESULTID ", " _MODULEID ", " _FILENAME ";";

static const char updatemodule[] =  "UPDATE " _MODULETABLENAME
                                    " SET " _TESTID " = $2, " _MODULETYPE " = $3, " _MODULETAG " = $4"
                                    " WHERE " _MODULEID " = $1"
                                    "RETURNING " _MODULEID ", " _TESTID ", " _MODULETYPE ", " _MODULETAG ";";

static const char updateresult[] =  "UPDATE " _RESULTTABLENAME
                                    " SET " _MODULEID " = $2, " _FILENAME " = $3"
                                    " WHERE " _RESULTID " = $1"
                                    "RETURNING " _RESULTID ", " _MODULEID ", " _FILENAME ";";

static const char updateresultdata[] =  "UPDATE " _RESULTTABLENAME
                                        " SET " _RESULTDATA " = $2"
                                        " WHERE " _RESULTID " = $1"
                                        "RETURNING " _RESULTID ", " _MODULEID ", " _FILENAME ";";

static const char deletetest[] =    "DELETE FROM " _TESTTABLENAME " WHERE " _TESTID " = $1;";

static const char deletemodule[] =  "DELETE FROM " _MODULETABLENAME " WHERE " _MODULEID " = $1;";

static const char deleteresult[] =  "DELETE FROM " _RESULTTABLENAME " WHERE " _RESULTID " = $1;";

static const char selecttest[] =    "SELECT " _TESTID ", " _TESTTIME
                                    " FROM " _TESTTABLENAME
                                    " WHERE " _TESTID " = $1;";

static const char selectmodule[] =  "SELECT " _MODULEID ", " _TESTID ", " _MODULETYPE ", " _MODULETAG
                                    " FROM " _MODULETABLENAME
                                    " WHERE " _MODULEID " = $1;";

static const char selectresult[] =  "SELECT " _RESULTID ", " _MODULEID ", " _FILENAME
                                    " FROM " _RESULTTABLENAME
                                    " WHERE " _RESULTID " = $1;";

static const char selectresultdata[] =  "SELECT " _RESULTDATA
                                        " FROM " _RESULTTABLENAME
                                        " WHERE " _RESULTID " = $1;";

static const char selecttests[] =   "SELECT " _TESTID ", " _TESTTIME
                                    " FROM " _TESTTABLENAME
                                    " OFFSET $1 LIMIT $2;";

static const char selecttestsnolimit[] =    "SELECT " _TESTID ", " _TESTTIME
                                            " FROM " _TESTTABLENAME
                                            " OFFSET $1;";

static const char selectmodules[] =  "SELECT " _MODULEID ", " _TESTID ", " _MODULETYPE ", " _MODULETAG
                                     " FROM " _MODULETABLENAME
                                     " OFFSET $1 LIMIT $2;";

static const char selectmodulesnolimit[] =  "SELECT " _MODULEID ", " _TESTID ", " _MODULETYPE ", " _MODULETAG
                                            " FROM " _MODULETABLENAME
                                            " OFFSET $1;";

static const char selectresults[] =  "SELECT " _RESULTID ", " _MODULEID ", " _FILENAME
                                     " FROM " _RESULTTABLENAME
                                     " OFFSET $1 LIMIT $2;";

static const char selectresultsnolimit[] =  "SELECT " _RESULTID ", " _MODULEID ", " _FILENAME
                                            " FROM " _RESULTTABLENAME
                                            " OFFSET $1;";

static const char selectmodulesfortests[] = "SELECT " _MODULEID ", " _TESTID ", " _MODULETYPE ", " _MODULETAG
                                            " FROM " _MODULETABLENAME
                                            " WHERE " _TESTID " = $1"
                                            " OFFSET $2 LIMIT $3;";

static const char selectmodulesfortestsnolimit[] =  "SELECT " _MODULEID ", " _TESTID ", " _MODULETYPE ", " _MODULETAG
                                                    " FROM " _MODULETABLENAME
                                                    " WHERE " _TESTID " = $1"
                                                    " OFFSET $2;";

static const char selectresultsformodule[] = "SELECT " _RESULTID ", " _MODULEID ", " _FILENAME
                                                    " FROM " _RESULTTABLENAME
                                                    " WHERE " _MODULEID " = $1"
                                                    " OFFSET $2 LIMIT $3;";

static const char selectresultsformodulenolimit[] = "SELECT " _RESULTID ", " _MODULEID ", " _FILENAME
                                                    " FROM " _RESULTTABLENAME
                                                    " WHERE " _MODULEID " = $1"
                                                    " OFFSET $2;";

static const char selectresultsfortest[] =   "SELECT " _RESULTID ", " _MODULETABLENAME "." _MODULEID ", " _FILENAME
                                             " FROM " _RESULTTABLENAME
                                             " JOIN " _MODULETABLENAME
                                             " ON " _MODULETABLENAME "." _MODULEID " = " _RESULTTABLENAME "." _MODULEID
                                             " WHERE " _TESTID " = $1"
                                             " OFFSET $2 LIMIT $3;";

static const char selectresultsfortestnolimit[] =   "SELECT " _RESULTID ", " _MODULETABLENAME "." _MODULEID ", " _FILENAME
                                                    " FROM " _RESULTTABLENAME
                                                    " JOIN " _MODULETABLENAME
                                                    " ON " _MODULETABLENAME "." _MODULEID " = " _RESULTTABLENAME "." _MODULEID
                                                    " WHERE " _TESTID " = $1"
                                                    " OFFSET $2;";


DBDriver::DBDriver():_connection(nullptr) {}

DBDriver::DBDriver(const std::string &host, int port, const std::string &dbname, const std::string &dbuser,
                   const std::string &dbpass) :
                   _connection(PQsetdbLogin(host.c_str(), std::to_string(port).c_str(), nullptr, nullptr,
                                            dbname.c_str(), dbuser.c_str(), dbpass.c_str()), &PQfinish)
{
    connect(host, port, dbname, dbuser, dbpass);
}

void DBDriver::connect(const std::string &host, int port, const std::string &dbname, const std::string &dbuser,
                       const std::string &dbpass)
{
    _connection.reset(PQsetdbLogin(host.c_str(), std::to_string(port).c_str(), nullptr, nullptr,
                             dbname.c_str(), dbuser.c_str(), dbpass.c_str()), &PQfinish);
    if(PQstatus(_connection.get()) == CONNECTION_BAD)
    {
        std::string msg = PQerrorMessage(_connection.get());
        _connection .reset();
        throw std::invalid_argument("Failed to connect to psql server: " + msg);
    }
}

bool DBDriver::isConnected() const
{
    return _connection.operator bool();
}

DBDriver::operator bool() const
{
    return _connection.operator bool();
}


bool DBDriver::tableExists(const char *tableName) const
{
    const char * tabnamearr[1] = {tableName};
    auto res = PQexecParams(_connection.get(), tableExistsQuery, 1, nullptr, tabnamearr, nullptr, nullptr, 1);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to check ") + tableName + " table existance: " +
                                 PQresultErrorMessage(res));
    }
    return static_cast<int>(*PQgetvalue(res, 0, 0)) != 0;
}

bool DBDriver::checkTableStructure(const char *tableName, const char **columns, size_t columnNum) const
{
    const char * tabnamearr[1] = {tableName};
    auto res = PQexecParams(_connection.get(), tableGetColumns, 1, nullptr, tabnamearr, nullptr, nullptr, 0);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to check ") + tableName + " table columns: " +
                                 PQresultErrorMessage(res));
    }
    for(size_t i = 0; i < columnNum; ++i)
    {
        bool found = false;
        for(int j = 0; PQgetvalue(res, j, 0) != nullptr; ++j )
        {
            if(strcmp(columns[i], PQgetvalue(res, j, 0)) == 0)
            {
                found = true;
                break;
            }
        }
        if(!found)
        {
            return false;
        }
    }
    return true;
}

void DBDriver::assertDBOk() const
{
    if(!_connection)
    {
        throw std::logic_error("assertDBOk called but not connected");
    }
    for(auto i : tables)
    {
        if(!tableExists(i))
        {
            throw std::runtime_error(std::string("Table ") + i + " does not exist");
        }
    }
    if(!checkTableStructure(_TESTTABLENAME, testcolumns, sizeof(testcolumns) / sizeof(*testcolumns)))
    {
        throw std::runtime_error("Table " _TESTTABLENAME " has invalid structure");
    }
    if(!checkTableStructure(_MODULETABLENAME, modulecolumns, sizeof(modulecolumns) / sizeof(*modulecolumns)))
    {
        throw std::runtime_error("Table " _MODULETABLENAME " has invalid structure");
    }
    if(!checkTableStructure(_RESULTTABLENAME, resultcolumns, sizeof(resultcolumns) / sizeof(*resultcolumns)))
    {
        throw std::runtime_error("Table " _RESULTTABLENAME " has invalid structure");
    }
}

void DBDriver::createTable(const char *tableName, const char *createQuery)
{
    if(!tableExists(tableName))
    {
        auto res = PQexec(_connection.get(), createQuery);
        if (PQresultStatus(res) != PGRES_COMMAND_OK)
        {
            throw std::runtime_error(std::string("Failed to create ") + tableName + " table: " +
                                     PQresultErrorMessage(res));
        }
    }
}

void DBDriver::createTables()
{
    createTable(_TESTTABLENAME, createtesttable);
    createTable(_MODULETABLENAME, createmoduletable);
    createTable(_RESULTTABLENAME, createresulttable);
}

DBDriver::test DBDriver::insertTest()
{
    if(!_connection)
    {
        throw std::logic_error("insertTest called but not connected");
    }
    auto res = PQexec(_connection.get(), inserttest);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to add row to ") + _TESTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    return {std::stoul(PQgetvalue(res, 0, 0)), PQgetvalue(res, 0, 1)};
}

DBDriver::module DBDriver::insertModule(const DBDriver::module &info)
{
    if(!_connection)
    {
        throw std::logic_error("insertModule called but not connected");
    }
    std::string testidstr = std::to_string(info.test_id);
    const char * params[] {testidstr.c_str(), info.module_type.c_str(), info.module_tag.c_str()};
    auto res = PQexecParams(_connection.get(), insertmodule, 3, nullptr, params, nullptr, nullptr, 0);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to add row to ") + _MODULETABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    return {std::stoul(PQgetvalue(res, 0, 0)),
            std::stoul(PQgetvalue(res, 0, 1)),
            PQgetvalue(res, 0, 2),
            PQgetvalue(res, 0, 3)};
}

DBDriver::result DBDriver::insertResult(const DBDriver::result &info, const std::vector<char> &data)
{
    if(!_connection)
    {
        throw std::logic_error("insertResult called but not connected");
    }
    std::string moduleidstr = std::to_string(info.module_id);
    const char *resparams[] = {moduleidstr.c_str(), info.file_name.c_str(), data.data()};
    const int lenparams[] = {0, 0, static_cast<int>(data.size())};
    const int typeparams[] = {0, 0, 1};
    auto res = PQexecParams(_connection.get(), insertresult, 3, nullptr, resparams, lenparams, typeparams, 0);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to add row to ") + _RESULTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    return {std::stoul(PQgetvalue(res, 0, 0)),
            std::stoul(PQgetvalue(res, 0, 1)),
            PQgetvalue(res, 0, 2)};
}

DBDriver::module DBDriver::updateModule(const DBDriver::module &info)
{
    if(!_connection)
    {
        throw std::logic_error("updateModule called but not connected");
    }
    std::string moduleidstr = std::to_string(info.module_id);
    std::string testidstr = std::to_string(info.test_id);
    const char * params[] {moduleidstr.c_str(), testidstr.c_str(), info.module_type.c_str(), info.module_tag.c_str()};
    auto res = PQexecParams(_connection.get(), updatemodule, 4, nullptr, params, nullptr, nullptr, 0);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to update row in ") + _MODULETABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    return {std::stoul(PQgetvalue(res, 0, 0)),
            std::stoul(PQgetvalue(res, 0, 1)),
            PQgetvalue(res, 0, 2),
            PQgetvalue(res, 0, 3)};
}

DBDriver::result DBDriver::updateResultInfo(const DBDriver::result &info)
{
    if(!_connection)
    {
        throw std::logic_error("updateResultInfo called but not connected");
    }
    std::string resultidstr = std::to_string(info.result_id);
    std::string moduleidstr = std::to_string(info.module_id);
    const char *resparams[] = {resultidstr.c_str(), moduleidstr.c_str(), info.file_name.c_str()};
    auto res = PQexecParams(_connection.get(), updateresult, 3, nullptr, resparams, nullptr, nullptr, 0);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to update row in ") + _RESULTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    return {std::stoul(PQgetvalue(res, 0, 0)),
            std::stoul(PQgetvalue(res, 0, 1)),
            PQgetvalue(res, 0, 2)};
}

DBDriver::result DBDriver::updateResultData(size_t result_id, const std::vector<char> &data)
{
    if(!_connection)
    {
        throw std::logic_error("updateResultData called but not connected");
    }
    std::string resultidstr = std::to_string(result_id);
    const char *resparams[] = {resultidstr.c_str(), data.data()};
    const int lenparams[] = {0, static_cast<int>(data.size())};
    const int typeparams[] = {0, 1};
    auto res = PQexecParams(_connection.get(), updateresultdata, 2, nullptr, resparams, lenparams, typeparams, 0);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to update data in ") + _RESULTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    return {std::stoul(PQgetvalue(res, 0, 0)),
            std::stoul(PQgetvalue(res, 0, 1)),
            PQgetvalue(res, 0, 2)};
}

void DBDriver::deleteTest(size_t test_id)
{
    if(!_connection)
    {
        throw std::logic_error("deleteTest called but not connected");
    }
    std::string idstr = std::to_string(test_id);
    const char * params[] {idstr.c_str()};
    auto res = PQexecParams(_connection.get(), deletetest, 1, nullptr, params, nullptr, nullptr, 0);
    if(PQresultStatus(res) != PGRES_COMMAND_OK && PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to delete row from ") + _TESTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
}

void DBDriver::deleteModule(size_t module_id)
{
    if(!_connection)
    {
        throw std::logic_error("deleteModule called but not connected");
    }
    std::string idstr = std::to_string(module_id);
    const char * params[] {idstr.c_str()};
    auto res = PQexecParams(_connection.get(), deletemodule, 1, nullptr, params, nullptr, nullptr, 0);
    if(PQresultStatus(res) != PGRES_COMMAND_OK && PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to delete row from ") + _MODULETABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
}

void DBDriver::deleteResult(size_t result_id)
{
    if(!_connection)
    {
        throw std::logic_error("deleteResult called but not connected");
    }
    std::string idstr = std::to_string(result_id);
    const char * params[] {idstr.c_str()};
    auto res = PQexecParams(_connection.get(), deleteresult, 1, nullptr, params, nullptr, nullptr, 0);
    if(PQresultStatus(res) != PGRES_COMMAND_OK && PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to delete row from ") + _RESULTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
}

DBDriver::test DBDriver::getTest(size_t test_id) const
{
    if(!_connection)
    {
        throw std::logic_error("getTest called but not connected");
    }
    std::string idstr = std::to_string(test_id);
    const char * params[] {idstr.c_str()};
    auto res = PQexecParams(_connection.get(), selecttest, 1, nullptr, params, nullptr, nullptr, 0);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to get row from ") + _TESTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    if(PQntuples(res) == 0)
    {
        throw std::runtime_error("Requested test does not exist");
    }
    return {std::stoul(PQgetvalue(res, 0, 0)), PQgetvalue(res, 0, 1)};
}

DBDriver::module DBDriver::getModule(size_t module_id) const
{
    if(!_connection)
    {
        throw std::logic_error("getModule called but not connected");
    }
    std::string idstr = std::to_string(module_id);
    const char * params[] {idstr.c_str()};
    auto res = PQexecParams(_connection.get(), selectmodule, 1, nullptr, params, nullptr, nullptr, 0);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to get row from ") + _MODULETABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }

    if(PQntuples(res) == 0)
    {
        throw std::runtime_error("Requested module does not exist");
    }
    return {std::stoul(PQgetvalue(res, 0, 0)),
            std::stoul(PQgetvalue(res, 0, 1)),
            PQgetvalue(res, 0, 2),
            PQgetvalue(res, 0, 3)};
}

DBDriver::result DBDriver::getResult(size_t result_id) const
{
    if(!_connection)
    {
        throw std::logic_error("getResult called but not connected");
    }
    std::string idstr = std::to_string(result_id);
    const char * params[] {idstr.c_str()};
    auto res = PQexecParams(_connection.get(), selectresult, 1, nullptr, params, nullptr, nullptr, 0);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to get row from ") + _RESULTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }

    if(PQntuples(res) == 0)
    {
        throw std::runtime_error("Requested result does not exist");
    }
    return {std::stoul(PQgetvalue(res, 0, 0)),
            std::stoul(PQgetvalue(res, 0, 1)),
            PQgetvalue(res, 0, 2)};
}

std::vector<char> DBDriver::getResultData(size_t result_id) const
{

    if(!_connection)
    {
        throw std::logic_error("getResultData called but not connected");
    }
    std::string idstr = std::to_string(result_id);
    const char * params[] {idstr.c_str()};
    auto res = PQexecParams(_connection.get(), selectresultdata, 1, nullptr, params, nullptr, nullptr, 1);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to data row from ") + _RESULTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    if(PQntuples(res) == 0)
    {
        throw std::runtime_error("Requested data does not exist");
    }
    const char* ptr = PQgetvalue(res, 0, 0);
    return std::vector<char>(ptr, ptr + PQgetlength(res, 0, 0));
}

std::vector<DBDriver::test> DBDriver::getTests(size_t offset, size_t limit) const
{
    if(!_connection)
    {
        throw std::logic_error("getTests called but not connected");
    }
    std::string offsetstr = std::to_string(offset);
    std::string limitstr = std::to_string(limit);
    const char * params[] {offsetstr.c_str(), limitstr.c_str()};
    PGresult* res;
    if(limit == unlimited)
    {
        res = PQexecParams(_connection.get(), selecttestsnolimit, 1, nullptr, params, nullptr, nullptr, 0);
    }
    else
    {
        res = PQexecParams(_connection.get(), selecttests, 2, nullptr, params, nullptr, nullptr, 0);
    }
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to get rows from ") + _TESTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    std::vector<test> ret {};
    ret.reserve(static_cast<unsigned long>(PQntuples(res)));
    for(int i = 0; i < PQntuples(res); ++i)
    {
        ret.push_back({std::stoul(PQgetvalue(res, i, 0)), PQgetvalue(res, i, 1)});
    }
    return ret;
}

std::vector<DBDriver::module> DBDriver::getModules(size_t offset, size_t limit) const
{
    if(!_connection)
    {
        throw std::logic_error("getModules called but not connected");
    }
    std::string offsetstr = std::to_string(offset);
    std::string limitstr = std::to_string(limit);
    const char * params[] {offsetstr.c_str(), limitstr.c_str()};
    PGresult *res;
    if(limit == unlimited)
    {
        res = PQexecParams(_connection.get(), selectmodulesnolimit, 1, nullptr, params, nullptr, nullptr, 0);
    }
    else
    {
        res = PQexecParams(_connection.get(), selectmodules, 2, nullptr, params, nullptr, nullptr, 0);
    }
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to get rows from ") + _MODULETABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    std::vector<module> ret {};
    ret.reserve(static_cast<unsigned long>(PQntuples(res)));
    for(int i = 0; i < PQntuples(res); ++i)
    {
        ret.push_back({std::stoul(PQgetvalue(res, i, 0)),
                       std::stoul(PQgetvalue(res, i, 1)),
                       PQgetvalue(res, i, 2),
                       PQgetvalue(res, i, 3)});
    }
    return ret;
}

std::vector<DBDriver::result> DBDriver::getResults(size_t offset, size_t limit) const
{
    if(!_connection)
    {
        throw std::logic_error("getResults called but not connected");
    }
    std::string offsetstr = std::to_string(offset);
    std::string limitstr = std::to_string(limit);
    const char * params[] {offsetstr.c_str(), limitstr.c_str()};
    PGresult * res;
    if(limit == unlimited)
    {
        res = PQexecParams(_connection.get(), selectresultsnolimit, 1, nullptr, params, nullptr, nullptr, 0);
    }
    else
    {
        res = PQexecParams(_connection.get(), selectresults, 2, nullptr, params, nullptr, nullptr, 0);
    }
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to get rows from ") + _RESULTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    std::vector<result> ret {};
    ret.reserve(static_cast<unsigned long>(PQntuples(res)));
    for(int i = 0; i < PQntuples(res); ++i)
    {
        ret.push_back({std::stoul(PQgetvalue(res, i, 0)),
                       std::stoul(PQgetvalue(res, i, 1)),
                       PQgetvalue(res, i, 2)});
    }
    return ret;
}

std::vector<DBDriver::module> DBDriver::getModulesForTest(size_t test_id, size_t offset, size_t limit) const
{
    if(!_connection)
    {
        throw std::logic_error("getModulesForTests called but not connected");
    }
    std::string idstr = std::to_string(test_id);
    std::string offsetstr = std::to_string(offset);
    std::string limitstr = std::to_string(limit);
    const char * params[] {idstr.c_str(), offsetstr.c_str(), limitstr.c_str()};
    PGresult *res;
    if(limit == unlimited)
    {
        res = PQexecParams(_connection.get(), selectmodulesfortestsnolimit, 2, nullptr, params, nullptr, nullptr, 0);
    }
    else
    {
        res = PQexecParams(_connection.get(), selectmodulesfortests, 3, nullptr, params, nullptr, nullptr, 0);
    }
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to get rows from ") + _MODULETABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    std::vector<module> ret {};
    ret.reserve(static_cast<unsigned long>(PQntuples(res)));
    for(int i = 0; i < PQntuples(res); ++i)
    {
        ret.push_back({std::stoul(PQgetvalue(res, i, 0)),
                       std::stoul(PQgetvalue(res, i, 1)),
                       PQgetvalue(res, i, 2),
                       PQgetvalue(res, i, 3)});
    }
    return ret;
}

std::vector<DBDriver::result> DBDriver::getResultsForTest(size_t test_id, size_t offset, size_t limit) const
{
    if(!_connection)
    {
        throw std::logic_error("getResultsForTest called but not connected");
    }
    std::string idstr = std::to_string(test_id);
    std::string offsetstr = std::to_string(offset);
    std::string limitstr = std::to_string(limit);
    const char * params[] {idstr.c_str(), offsetstr.c_str(), limitstr.c_str()};
    PGresult * res;
    if(limit == unlimited)
    {
        res = PQexecParams(_connection.get(), selectresultsfortestnolimit, 2, nullptr, params, nullptr, nullptr, 0);
    }
    else
    {
        res = PQexecParams(_connection.get(), selectresultsfortest, 3, nullptr, params, nullptr, nullptr, 0);
    }
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to get rows from ") + _RESULTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    std::vector<result> ret {};
    ret.reserve(static_cast<unsigned long>(PQntuples(res)));
    for(int i = 0; i < PQntuples(res); ++i)
    {
        ret.push_back({std::stoul(PQgetvalue(res, i, 0)),
                       std::stoul(PQgetvalue(res, i, 1)),
                       PQgetvalue(res, i, 2)});
    }
    return ret;
}

std::vector<DBDriver::result> DBDriver::getResultsForModule(size_t module_id, size_t offset, size_t limit) const
{
    if(!_connection)
    {
        throw std::logic_error("getResultsForModule called but not connected");
    }
    std::string idstr = std::to_string(module_id);
    std::string offsetstr = std::to_string(offset);
    std::string limitstr = std::to_string(limit);
    const char * params[] {idstr.c_str(), offsetstr.c_str(), limitstr.c_str()};
    PGresult * res;
    if(limit == unlimited)
    {
        res = PQexecParams(_connection.get(), selectresultsformodulenolimit, 2, nullptr, params, nullptr, nullptr, 0);
    }
    else
    {
        res = PQexecParams(_connection.get(), selectresultsformodule, 3, nullptr, params, nullptr, nullptr, 0);
    }
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        throw std::runtime_error(std::string("Failed to get rows from ") + _RESULTTABLENAME + " table: " +
                                 PQresultErrorMessage(res));
    }
    std::vector<result> ret {};
    ret.reserve(static_cast<unsigned long>(PQntuples(res)));
    for(int i = 0; i < PQntuples(res); ++i)
    {
        ret.push_back({std::stoul(PQgetvalue(res, i, 0)),
                       std::stoul(PQgetvalue(res, i, 1)),
                       PQgetvalue(res, i, 2)});
    }
    return ret;
}
