#ifndef PRNG_MANAGER_DBDRIVER_H
#define PRNG_MANAGER_DBDRIVER_H

#include <memory>
#include <vector>

class pg_conn;

class DBDriver
{
public:
    struct test
    {
        size_t test_id;
        std::string submission_time;
    };
    struct module
    {
        size_t module_id;
        size_t test_id;
        std::string module_type;
        std::string module_tag;
    };
    struct result
    {
        size_t result_id;
        size_t module_id;
        std::string file_name;
    };
private:
    std::shared_ptr<pg_conn> _connection;
    bool tableExists(const char *tableName) const;
    bool checkTableStructure(const char * tableName, const char** columns, size_t columnNum) const;
    void createTable(const char * tableName, const char* createQuery);
public:
    DBDriver();
    DBDriver(const std::string & host, int port, const std::string & dbname, const std::string & dbuser,
             const std::string & dbpass = "");
    void connect(const std::string & host, int port, const std::string & dbname, const std::string & dbuser,
                 const std::string & dbpass = "");
    bool isConnected() const;
    operator bool() const;
    void assertDBOk() const;
    void createTables();
    test insertTest();
    module insertModule(const module &info);
    result insertResult(const result &info, const std::vector<char> &data = {});

    module updateModule(const module &info);
    result updateResultInfo(const result &info);
    result updateResultData(size_t result_id, const std::vector<char> &data);
    inline result updateResultData(const result &info, const std::vector<char> &data)
    {return updateResultData(info.result_id, data);}


    void deleteTest(size_t test_id);
    inline void deleteTest(const test &info) { deleteTest(info.test_id);}
    void deleteModule(size_t module_id);
    inline void deleteModule(const module &info) { deleteModule(info.module_id);}
    void deleteResult(size_t result_id);
    inline void deleteResult(const result &info) { deleteResult(info.result_id);}

    test getTest(size_t test_id) const;
    module getModule(size_t module_id) const;
    result getResult(size_t result_id) const;
    std::vector<char> getResultData(size_t result_id) const;
    inline std::vector<char> getResultData(const result & info) { return getResultData(info.result_id);}

    enum limit : size_t
    {
        unlimited = 0 ///< Get all rows
    };

    std::vector<test> getTests(size_t offset, size_t limit) const;
    std::vector<module> getModules(size_t offset, size_t limit) const;
    std::vector<result> getResults(size_t offset, size_t limit) const;

    std::vector<module> getModulesForTest(size_t test_id, size_t offset = 0, size_t limit = unlimited) const;
    inline std::vector<module> getModulesForTest(const test & info, size_t offset = 0, size_t limit = unlimited) const
    { return getModulesForTest(info.test_id, offset, limit);}
    std::vector<result> getResultsForTest(size_t test_id, size_t offset = 0, size_t limit = unlimited) const;
    inline std::vector<result> getResultsForTest(const test & info, size_t offset = 0, size_t limit = unlimited) const
    { return getResultsForTest(info.test_id, offset, limit);}
    std::vector<result> getResultsForModule(size_t module_id, size_t offset = 0, size_t limit = unlimited) const;
    inline std::vector<result> getResultsForModule(const module & info, size_t offset = 0, size_t limit = unlimited) const
    { return getResultsForModule(info.module_id, offset, limit);}



    inline test getTestOfModule(size_t module_id) const { return getTest(getModule(module_id).test_id);}
    inline test getTestOfModule(const module & info) const { return getTest(info.test_id);}
    inline test getTestOfResult(size_t result_id) const
    { return getTest(getModule(getResult(result_id).module_id).test_id);}
    inline test getTestOfResult(const result & info) const { return getTest(getModule(info.module_id).test_id);}

    inline module getModuleOfResult(size_t result_id) const { return getModule(getResult(result_id).module_id);}
    inline module getModuleOfResult(const result & info) const { return getModule(info.module_id);}
};


#endif //PRNG_MANAGER_DBDRIVER_H
