//
// Created by svuatoslav on 11/21/18.
//

#include "ExecIface.h"

std::vector<std::string> ExecIface::getReqVals ()
{
    return std::vector<std::string> {"Arguments"};
}

void ExecIface::execute (const std::unordered_map<std::string, std::string> &vals)
{
    if(vals.find ("Arguments") == vals.end())
    {
        execute ();
    }
    else
    {
        execute (vals.at ("Arguments"));
    }
}

void ExecIface::execute (const std::string &parameters)
{
    if(_executable.empty ())
    {
        throw std::logic_error("Executable file was not set before executing");
    }
    std::string command = _executable + " " + parameters;
    int res = system (command.c_str ());
    if(res == -1)
    {
        throw std::runtime_error("Failed to launch shell for command \"" + command + "\"");
    }
    if(res == 0x7f00)
    {
        throw std::runtime_error("Failed to execute command \"" + command + "\"");
    }
    else if(res != 0)
    {
        processError (command, res);
    }
}

void ExecIface::processError (const std::string & command, int retcode)
{
    char res[7];
    sprintf (res, "0x%04x", retcode);
    throw std::runtime_error("Command \"" + command + "\" returned non zero status : " + res);
}

void ExecIface::setExecutable (const std::string &executable)
{
    _executable = executable;
}

ExecIface::ExecIface (const std::string & executable) : _executable(executable) {}
