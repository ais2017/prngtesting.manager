//
// Created by svuatoslav on 11/21/18.
//

#ifndef PRNG_MANAGER_EXECIFACE_H
#define PRNG_MANAGER_EXECIFACE_H


#include <ModuleInterface.h>

class ExecIface : public ModuleInterface
{
    std::string _executable;

public:

    explicit ExecIface(const std::string & executable = "");

    ~ExecIface () override = default;

    void setExecutable(const std::string & executable);

    std::vector<std::string> getReqVals () override;

    void execute (const std::string &parameters = "");

    void execute (const std::unordered_map<std::string, std::string> &vals) override;

    virtual void processError(const std::string & command, int retcode);

};


#endif //PRNG_MANAGER_EXECIFACE_H
