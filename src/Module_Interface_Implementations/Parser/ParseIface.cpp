//
// Created by malaschuk on 11/29/18.
//

#include "ParseIface.h"
#include <fstream>
#include <string>
#include <cstring>

std::string arg = "CommandFile", in = "LogFile";

ParseIface::ParseIface (const std::string &executable) : ExecIface(executable) {}

std::vector<std::string> ParseIface::getReqVals ()
{
    return std::vector<std::string> {arg, in};
}

void ParseIface::execute (const std::unordered_map<std::string, std::string> &vals)
{
    if(vals.find (arg) == vals.end() || vals.at(arg).empty())
    {
        throw std::invalid_argument(arg + std::string(" is not specified"));
    }
    else
    {
        if(vals.find (in) == vals.end() || vals.at(in).empty())
        {
            throw std::invalid_argument(in + std::string(" is not specified"));
        }
        else
        {
            testOutputFile(in, vals.at (in));
            testCommandFile(arg, vals.at (arg));
            ExecIface::execute (vals.at (arg) + ' ' + vals.at (in));
        }
    }
}

void ParseIface::processError(const std::string &command, int retcode)
{
    switch (retcode)
    {
        case 0x0000:
            break;
        case 0x0100:
            throw std::runtime_error("Parser interface (\"" + command + "\") failed: error opening input or output files");
        case 0x0200:
            throw std::runtime_error("Parser interface (\"" + command + "\") failed: invalid parameter set");
        default:
            char res[7];
            sprintf (res, "0x%04x", retcode);
            throw std::runtime_error("Parser interface (\"" + command + "\") failed: unknown error " + res);
    }
}

void ParseIface::testCommandFile (const std::string &key, const std::string &value)
{
    std::fstream stream(value, std::ios_base::in);
    if(!stream)
    {
        throw std::invalid_argument(key + " ( \"" += value + "\" ) " + "can not be accessed for read: "
                                        + strerror (errno));
    }
}

void ParseIface::testOutputFile (const std::string &key, const std::string &value)
{
    std::fstream stream(value, std::ios_base::app);
    if(!stream)
    {
        throw std::invalid_argument(key + " ( \"" += value + "\" ) " + "can not be accessed for write: "
                                        + strerror (errno));
    }
}
