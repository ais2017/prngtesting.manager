//
// Created by malaschuk on 11/29/18.
//

#ifndef PRNG_MANAGER_PARSEIFACE_H
#define PRNG_MANAGER_PARSEIFACE_H


#include <Executable/ExecIface.h>

class ParseIface: public ExecIface
{
    static void testCommandFile(const std::string &key, const std::string& value);
    static void testOutputFile(const std::string &key, const std::string& value);

public:

    explicit ParseIface(const std::string & executable = "");

    ~ParseIface () override = default;

    std::vector<std::string> getReqVals () override;

    void execute (const std::unordered_map<std::string, std::string> &vals) override;

    void processError(const std::string &command, int retcode) override;
};


#endif //PRNG_MANAGER_PARSEIFACE_H
