//
// Created by svuatoslav on 12/3/18.
//

#include <fstream>
#include "DBSaver.h"
#include <unordered_set>




DBSaver::DBSaver(const std::string & host, int port, const std::string & dbname, const std::string & dbuser,
        const std::string & dbpass,
        const std::vector<std::tuple<std::string, std::string, std::vector<std::string>>> & files) :
        _driver(host, port, dbname, dbuser, dbpass), files(files)
{
    _driver.createTables();
}

std::vector<std::string> DBSaver::getReqVals()
{
    return {"IncludeTags", "ExcludeTags"};
}

void DBSaver::execute(const std::unordered_map<std::string, std::string> & params)
{
    if(!_driver)
    {
        throw std::logic_error("Not connected to psql server");
    }
    std::unordered_set<std::string> includes {};
    std::unordered_set<std::string> excludes {};
    {
        auto includeparam = params.find("IncludeTags");
        if (includeparam != params.cend() && !includeparam->second.empty())
        {
            const std::string &includestr = includeparam->second;
            for (unsigned long pos = includestr.find(' ', 0), ppos = 0; pos != std::string::npos;
                 ppos = pos, pos = includestr.find(' ', pos + 1))
            {
                if (pos > ppos + 1)
                {
                    includes.emplace(includestr.substr(ppos, pos - ppos));
                }
            }
        }
    }
    {
        auto excludeparam = params.find("ExcludeTags");
        if (excludeparam != params.cend() && !excludeparam->second.empty())
        {
            const std::string &excludestr = excludeparam->second;
            for (unsigned long pos = excludestr.find(' ', 0), ppos = 0; pos != std::string::npos;
                 ppos = pos, pos = excludestr.find(' ', pos + 1))
            {
                if (pos > ppos + 1)
                {
                    excludes.emplace(excludestr.substr(ppos, pos - ppos));
                }
            }
        }
    }
    _driver.assertDBOk();
    auto testinfo = _driver.insertTest();
    for(auto & i : files)
    {
        if((includes.find(std::get<1>(i)) != includes.cend()) ||
           (includes.empty() && (excludes.find(std::get<1>(i)) == excludes.cend())))
        {
            auto moduleinfo = _driver.insertModule({0, testinfo.test_id, std::get<0>(i), std::get<1>(i)});
            for (auto &filename : std::get<2>(i))
            {
                std::ifstream in(filename.data(), std::ios::binary | std::ios::ate);
                if (!in.good())
                {
                    continue;
                }
                std::streamsize fsize = in.tellg();
                in.seekg(0, std::ios::beg);
                std::vector<char> filedata(static_cast<unsigned long>(fsize));
                if (!in.read(filedata.data(), fsize))
                {
                    continue;
                }
                _driver.insertResult({0, moduleinfo.module_id, filename}, filedata);
            }
        }
    }
}