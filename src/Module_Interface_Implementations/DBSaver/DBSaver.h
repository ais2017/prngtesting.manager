//
// Created by svuatoslav on 12/3/18.
//

#ifndef PRNG_MANAGER_DBSAVER_H
#define PRNG_MANAGER_DBSAVER_H

#include <vector>
#include <string>
#include "../DBDriver/DBDriver.h"
#include <ModuleInterface.h>

class DBSaver : public ModuleInterface
{
    DBDriver _driver;
    std::vector<std::tuple<std::string, std::string, std::vector<std::string>>> files;

public:
    DBSaver(const std::string & host, int port, const std::string & dbname, const std::string & dbuser,
            const std::string & dbpass,
            const std::vector<std::tuple<
                            std::string,               //Module type
                            std::string,               //Module tag
                            std::vector<std::string>   //Files
                            >> & files);

    ~DBSaver() override = default;

    std::vector<std::string> getReqVals() override;

    void execute(const std::unordered_map<std::string, std::string> &vals) override;
};


#endif //PRNG_MANAGER_DBSAVER_H
