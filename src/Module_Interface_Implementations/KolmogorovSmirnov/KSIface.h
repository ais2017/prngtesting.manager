//
// Created by svuatoslav on 11/21/18.
//

#ifndef PRNG_MANAGER_KSIFACE_H
#define PRNG_MANAGER_KSIFACE_H


#include <Executable/ExecIface.h>

class KSIface: public ExecIface
{

    static void testLength(const std::string &key, const std::string& value);
    static void testFractional (const std::string &key, const std::string &value);
    static void testOutputFile(const std::string &key, const std::string& value);
    static void testInputFiles(const std::string &key, const std::string& value);

public:

    explicit KSIface(const std::string & executable = "");

    ~KSIface () override = default;

    std::vector<std::string> getReqVals () override;

    void execute (const std::unordered_map<std::string, std::string> &vals) override;

    void processError(const std::string &command, int retcode) override;
};


#endif //PRNG_MANAGER_KSIFACE_H
