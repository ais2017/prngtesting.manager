//
// Created by svuatoslav on 11/21/18.
//

#include "KSIface.h"

#include <fstream>
#include <cstring>

const char* arguments[] = {"ValueLength", "SpaceLength", "Significance", "OutputFile", "InputFiles"};

enum ValueType
{
    NoTest,
    Length,
    Fractional,
    OutputFile,
    InputFiles
};

const ValueType argTypes[sizeof (arguments) / sizeof (*arguments)] =
        {Length, Length, Fractional, OutputFile, InputFiles};

std::vector<std::string> KSIface::getReqVals ()
{
    return std::vector<std::string>(arguments, arguments + sizeof (arguments) / sizeof (*arguments));
}

void KSIface::execute (const std::unordered_map<std::string, std::string> &vals)
{
    std::string params;
    for(size_t i = 0; i < sizeof (arguments) / sizeof (*arguments); ++i)
    {
        if(vals.find (arguments[i]) == vals.end() || vals.at (arguments[i]).empty())
        {
            throw std::invalid_argument(arguments[i] + std::string(" is not specified"));
        }
        switch (argTypes[i])
        {
            case Length:
                testLength (arguments[i], vals.at (arguments[i]));
                break;
            case Fractional:
                testFractional (arguments[i], vals.at (arguments[i]));
                break;
            case OutputFile:
                testOutputFile (arguments[i], vals.at (arguments[i]));
                break;
            case InputFiles:
                testInputFiles (arguments[i], vals.at (arguments[i]));
                break;
            default:
                break;
        }
        params += ( i == 0? vals.at (arguments[i]) : std::string(" ") + vals.at(arguments[i]) );
    }
    ExecIface::execute (params);
}

void KSIface::testLength (const std::string &key, const std::string &value)
{
    long testVal;
    try
    {
        testVal = std::stol (value);
    }
    catch (...)
    {
        throw std::invalid_argument(key + " ( \"" + value + "\" ) " + "can't be converted to an integer value");
    }
    if(testVal <= 0 )
    {
        throw std::invalid_argument(key + " ( \"" + value + "\" interpreted as " + std::to_string (testVal) + " ) "
                                        + "must be higher than zero");
    }
}

void KSIface::testFractional (const std::string &key, const std::string &value)
{
    float testVal;
    try
    {
        testVal = std::stof (value);
    }
    catch (...)
    {
        throw std::invalid_argument(key + " ( \"" + value + "\" ) " + "can't be converted to a fractional number");
    }
    if(testVal < 0 || testVal >= 1 )
    {
        throw std::invalid_argument(key + " ( \"" + value + "\" interpreted as " + std::to_string (testVal) + " ) "
                                    + "must take values from range [ 0 ; 1 )");
    }
}

void KSIface::testOutputFile (const std::string &key, const std::string &value)
{
    std::fstream stream(value, std::ios_base::app);
    if(!stream)
    {
        throw std::invalid_argument(key + " ( \"" += value + "\" ) " + "can not be accessed for write: "
                                        + strerror (errno));
    }
}

void KSIface::testInputFiles (const std::string &key, const std::string &value)
{
    for(size_t pos = 0; pos != std::string::npos;)
    {
        size_t npos = value.find (' ', pos);
        std::string filename = value.substr (pos, npos - pos);
        if(!filename.empty ())
        {
            std::fstream stream(filename, std::ios_base::in);
            if(!stream)
            {
                throw std::invalid_argument(key + " ( \"" += value + "\" currently on position "
                += std::to_string (pos) + " with " += filename + " ) " + "can not be accessed for read: "
                + strerror (errno));
            }
        }
        pos = ( npos == std::string::npos? npos : npos + 1 );
    }
}

KSIface::KSIface (const std::string &executable) : ExecIface(executable) {}

void KSIface::processError(const std::string &command, int retcode)
{
    switch (retcode)
    {
        case 0x0000:
            break;
        case 0x0100:
            throw std::runtime_error("Kolmogorov-Smirnov interface (\"" + command + "\") failed: bad parameters");
        case 0x0200:
            throw std::runtime_error("Kolmogorov-Smirnov interface (\"" + command + "\") failed: bad size or separator");
        case 0x0300:
            throw std::runtime_error("Kolmogorov-Smirnov interface (\"" + command + "\") failed: bad sequence files");
        case 0x0400:
            throw std::runtime_error("Kolmogorov-Smirnov interface (\"" + command + "\") failed: bad significance level");
        default:
            char res[7];
            sprintf (res, "0x%04x", retcode);
            throw std::runtime_error("Kolmogorov-Smirnov interface (\"" + command + "\") failed: unknown error " + res);
    }
}
