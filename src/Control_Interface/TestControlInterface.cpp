//
// Created by svuatoslav on 11/4/18.
//

#include "TestControlInterface.h"

void TestControlInterface::notify (size_t)
{
    ++_notificationsNum;
}

size_t TestControlInterface::getNotifications () const
{
    return _notificationsNum;
}
