//
// Created by svuatoslav on 11/4/18.
//

#ifndef PRNG_MANAGER_TESTCOMMANDINTERFACE_H
#define PRNG_MANAGER_TESTCOMMANDINTERFACE_H

#include "ControlInterface.h"
#include <queue>

class TestControlInterface : public ControlInterface
{
public:

private:

    size_t _notificationsNum = 0;

public:

    size_t getNotifications() const;

    void notify (size_t count) override;

    ~TestControlInterface () override = default;

};


#endif //PRNG_MANAGER_TESTCOMMANDINTERFACE_H
