//
// Created by svuatoslav on 11/4/18.
//

#ifndef PRNG_MANAGER_CONTROLINTERFACE_H
#define PRNG_MANAGER_CONTROLINTERFACE_H

#include <vector>
#include <string>
#include <unordered_map>

class ControlInterface
{
public:

    virtual void notify(size_t count) = 0;
    virtual ~ControlInterface () = default;
};

#endif //PRNG_MANAGER_COMMANDINTERFACE_H
