//
// Created by svuatoslav on 11/4/18.
//

#ifndef PRNG_MANAGER_TESTSTORAGEINTERFACE_H
#define PRNG_MANAGER_TESTSTORAGEINTERFACE_H


#include "StorageInterface.h"

class TestStorageInterface : public StorageInterface
{
    std::vector<std::pair<int, int>> _confs;
    std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> _mods;
    std::pair<int, std::vector<std::pair<std::string, std::string>>> _saveMod;

public:
    ~TestStorageInterface () override = default;

    void writeModulesConfig (const std::vector<std::pair<int, int>> &conf) override;

    void
    writeModules (const std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> &mods) override;

    void writeSaveModule (const std::pair<int, std::vector<std::pair<std::string, std::string>>> &module) override;

    std::vector<std::pair<int, int>> getModuleSettings () const override;

    std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> getModules () const override;

    std::pair<int, std::vector<std::pair<std::string, std::string>>> getSaveModule () const override;
};


#endif //PRNG_MANAGER_TESTSTORAGEINTERFACE_H
