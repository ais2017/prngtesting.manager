//
// Created by svuatoslav on 11/4/18.
//

#include "TestStorageInterface.h"

void TestStorageInterface::writeModulesConfig (const std::vector<std::pair<int, int>> &conf)
{
    _confs = conf;
}

void TestStorageInterface::writeModules (
        const std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> &mods)
{
    _mods = mods;
}

void
TestStorageInterface::writeSaveModule (const std::pair<int, std::vector<std::pair<std::string, std::string>>> &module)
{
    _saveMod = module;
}

std::vector<std::pair<int, int>> TestStorageInterface::getModuleSettings () const
{
    return _confs;
}

std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> TestStorageInterface::getModules () const
{
    return _mods;
}

std::pair<int, std::vector<std::pair<std::string, std::string>>> TestStorageInterface::getSaveModule () const
{
    return _saveMod;
}
