cmake_minimum_required(VERSION 3.7)
project(prng.manager)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-Wall -Wextra" )

add_library(test_storage_interface TestStorageInterface.h TestStorageInterface.cpp StorageInterface.h)