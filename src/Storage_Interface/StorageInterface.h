//
// Created by svuatoslav on 11/4/18.
//

#ifndef PRNG_MANAGER_STORAGEINTERFACE_H
#define PRNG_MANAGER_STORAGEINTERFACE_H

#include <vector>
#include <string>

class StorageInterface
{
public:
    virtual ~StorageInterface () = default;

    virtual void writeModulesConfig(const std::vector<std::pair<int, int>> &conf) = 0;
    virtual void writeModules(const std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> &mods) = 0;
    virtual void writeSaveModule(const std::pair<int, std::vector<std::pair<std::string, std::string>>> & module) = 0;

    virtual std::vector<std::pair<int, int>> getModuleSettings() const = 0;
    virtual std::vector<std::pair<int, std::vector<std::pair<std::string, std::string>>>> getModules() const = 0;
    virtual std::pair<int, std::vector<std::pair<std::string, std::string>>> getSaveModule() const = 0;
};

#endif //PRNG_MANAGER_STORAGEINTERFACE_H
