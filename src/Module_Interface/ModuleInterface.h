//
// Created by svuatoslav on 11/4/18.
//

#ifndef MANAGER_MODULTEINTERFACE_H
#define MANAGER_MODULTEINTERFACE_H

#include <unordered_map>
#include <memory>
#include <vector>

class ModuleInterface
{
public:
    virtual ~ModuleInterface () = default;
    virtual std::vector<std::string> getReqVals() = 0;
    virtual void execute(const std::unordered_map<std::string, std::string> &vals) = 0;
};

#endif //MANAGER_MODULTEINTERFACE_H
