//
// Created by svuatoslav on 11/4/18.
//

#include <unordered_map>
#include "TestModuleInterface.h"

std::vector<std::string> TestModuleInterface::getReqVals ()
{
    std::vector<std::string> ret(2);
    ret[0] = "TestVal";
    ret[1] = "WannaThrow?";
    return ret;
}

void TestModuleInterface::execute (const std::unordered_map<std::string, std::string> &vals)
{
    if(vals.find ("TestVal") != vals.end () && !vals.at ("TestVal").empty())
    {

        int val;
        try
        {
            val = std::stoi(vals.at ("TestVal"));
        }
        catch(std::invalid_argument & ex)
        {
            return;
        }
        catch(std::out_of_range & ex)
        {
            return;
        }
        tested.push_back (val);
    }
    if(vals.find ("WannaThrow?") != vals.end () && vals.at("WannaThrow?") == "YesPlease!")
    {

        throw std::invalid_argument("YouAreWelcome!");
    }
}

const std::vector<int> &TestModuleInterface::getTested ()
{
    return tested;
}
