//
// Created by svuatoslav on 11/4/18.
//

#ifndef PRNG_MANAGER_TESTMODULEINTERFACE_H
#define PRNG_MANAGER_TESTMODULEINTERFACE_H

#include <unordered_map>
#include "ModuleInterface.h"

class TestModuleInterface : public ModuleInterface
{
    std::vector<int> tested;
public:

    ~TestModuleInterface () override = default;

    std::vector<std::string> getReqVals () override;

    void execute (const std::unordered_map<std::string, std::string> &vals) override;

    const std::vector<int>& getTested();

};


#endif //PRNG_MANAGER_TESTMODULEINTERFACE_H
